import os
import datetime
import argparse
from setproctitle import setproctitle
from pytz import timezone

from utils.log_module import LogConfig, get_log_view
from utils.code_utils import convert
# from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul

import warnings
warnings.filterwarnings(action="ignore")


def train_openstl(cfg, log, err_log):
#     from src.openstl_preprocess import SatPrep
#     from src.openstl_model import OpenSTLModel
#     from src.openstl_train import SatTrain

#     log.info("--------------------------------------------------")
#     err_log.error("--------------------------------------------------")
    
#     # Preprocess
#     prep = SatPrep(cfg, log, err_log)
#     train_dl, valid_dl, mean_std = prep()
    
#     # Make Model & Save Adapted configs
#     model = OpenSTLModel(cfg, log, err_log)
#     args = model("train")
    
#     # Train Model
#     trn = SatTrain(cfg, log, err_log)
#     trn(args, train_dl, valid_dl, valid_dl, prep, mean_std)
    from src.openstl_preprocess import SatPrepV2
    from src.openstl_model import OpenSTLModel
    from src.openstl_train import SatTrain

    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Preprocess
    prep = SatPrepV2(cfg, log, err_log)
    train_dl, valid_dl, normalizer_args = prep()
    mean_std = (normalizer_args["mean"], normalizer_args["std"])
    
    # Make Model & Save Adapted configs
    model = OpenSTLModel(cfg, log, err_log)
    args = model("train")
    
    # Train Model
    trn = SatTrain(cfg, log, err_log)
    trn(args, train_dl, valid_dl, valid_dl, prep, mean_std)


def test(cfg, log, err_log):
    pass


def predict_openstl(kst_start_time, kst_end_time, cfg, log, err_log):
    from src.openstl_preprocess import SatPrep
    from src.openstl_model import OpenSTLModel
    from src.openstl_inference import SatInfer

    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Preprocess
    prep = SatPrep(cfg, log, err_log, pred_mode=True)
    infer_dl, utc_time_list = prep(kst_start_time, kst_end_time)
    
    # Make Model & Save Adapted configs
    model = OpenSTLModel(cfg, log, err_log)
    args = model("test")
    
    # Predict & Save satellite timeseries data
    infer = SatInfer(cfg, log, err_log, prep)
    openstl = infer.make_baseinstance(args)
    openstl = infer.load_trained(openstl, infer.load_model_dir)
    infer(args, openstl, infer_dl, utc_time_list)


def validate(kst_start_time, kst_end_time, cfg, log, err_log):
    pass


def make_image(kst_start_time, kst_end_time, cfg, log, err_log):
    # from src.make_img import SatImg
    from src.openstl_make_img import SatImg

    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Make & Save predicted images
    img = SatImg(cfg, log, err_log)
    img(kst_start_time, kst_end_time)


def data_extract(cfg, log, err_log):
    from src.extract_data import GK2ADataExtractor
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    kst_start_time_list = cfg.RAWDATA.KST_START_TIME
    kst_end_time_list = cfg.RAWDATA.KST_END_TIME
    time_step = cfg.RAWDATA.TIME_STEP
    
    # Make GK2A.dat
    sat = GK2ADataExtractor(cfg, log, err_log)
    for kst_start_time, kst_end_time in zip(kst_start_time_list, kst_end_time_list):
        sat_array, file_kst_end_time = sat(kst_start_time, kst_end_time, time_step)
        sat.save_sat_ary(sat_array, kst_start_time, file_kst_end_time)


def setting_log(cfg,
                upper_dir,
                chn,
                log_level="INFO",
                err_log_level="WARNING"):
    """
    Description:
        로그 객체 생성.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
        upper_dir: str, 해당 파일의 상위 디렉토리 경로
        chn: str, 위성 채널
        log_level: str, 로그 레벨 (default="INFO")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
        err_log_level: str, 에러 로그 레벨 (default="WARNING")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
    Returns:
        log: logging.logger, 로그 객체
        err_log: logging.logger, 에러 로그 객체
    """
    file_prefix = cfg.LOG_PREFIX.format(chn)
    lc = LogConfig(upper_dir, file_prefix)
    log = get_log_view(lc, log_level=log_level)   # default: INFO
    err_log = get_log_view(lc, log_level=err_log_level, error_log=True)
    return log, err_log


def main(args):
    """
    Description:
        메인 모듈 실행함수.
        data 때는 tensorflow import 하지 않도록 분기 설정.
        valid, image, data 모드일 땐, start_time과 end_time 설정이 필수.
        사용 예: python main.py -c ./configs/sat_ir087_pred_cfg.yaml -m train
                 python main.py -c ./configs/sat_ir087_pred_cfg.yaml -m valid -st 202203150700 -et 202203150850
    Args:
        args: 파이썬 실행할 때, 입력한 값
              "-c", "--config": 설정 yaml 파일 경로
              "-m", "--mode": train, test, predict, image, data 모드 변경
              "-st", "--start_time", 추론 시작 연월일시분 (KST), e.g. 202203150700
              "-et", "--end_time", 추론 마지막 연월일시분 (KST), e.g. 202203150850
    """
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    cfg_file = args.config
    cfg = convert(cfg_file)
    chn = cfg.PARAMS.DATA.CHANNEL
    
    if args.mode in ["train", "test", "predict"]:
        # import torch
        # from utils.tf_settings import set_gpu, set_gpu_growth
        from utils.torch_settings import set_random_seed, set_float32_matmul
        
        # tensorflow 설정
        # os.environ["LD_PRELOAD"] = "/usr/lib/x86_64-linux-gnu/libtcmalloc_minimal.so.4"   # tcmalloc 사용
        # set_gpu(cfg.SETTINGS.IDX_GPUS, cfg.SETTINGS.GPU_MEMORY)
        # set_gpu_growth(cfg.SETTINGS.IDX_GPUS)
        
        # torch 설정
        os.environ["CUDA_VISIBLE_DEVICES"] = ",".join([str(i) for i in cfg.SETTINGS.IDX_GPUS])
        os.environ["RANK"] = "0"
        # torch.multiprocessing.set_start_method("spawn")
        # device = set_gpu_torch(cfg.SETTINGS.IDX_GPUS[0], cfg.SETTINGS.GPU_MEMORY)
        set_random_seed(cfg.SETTINGS.RANDOM_SEED, if_cudnn=True)
        # set_float32_matmul("high")
        set_float32_matmul()
    
    if args.mode == "train":
        setproctitle("sat_{0}_train".format(chn))
        log, err_log = setting_log(cfg.TRAIN, upper_dir, chn)
        train_openstl(cfg, log, err_log)
        
    elif args.mode == "test":
        setproctitle("sat_{0}_test".format(chn))
        log, err_log = setting_log(cfg.TEST, upper_dir, chn)
        test(cfg, log, err_log)
        
    elif args.mode == "predict":
        setproctitle("sat_{0}_pred".format(chn))
        log, err_log = setting_log(cfg.INFER, upper_dir, chn)
        
        kst_start_time = args.start_time
        kst_end_time = args.end_time if args.end_time is not None else kst_start_time
        predict_openstl(kst_start_time, kst_end_time, cfg, log, err_log)
    
    elif args.mode == "valid":
        setproctitle("sat_{0}_valid".format(chn))
        log, err_log = setting_log(cfg.VALID, upper_dir, chn)
        
        kst_start_time = args.start_time
        kst_end_time = args.end_time if args.end_time is not None else kst_start_time
        validate(kst_start_time, kst_end_time, cfg, log, err_log)

    elif args.mode == "image":
        setproctitle("sat_{0}_img".format(chn))
        log, err_log = setting_log(cfg.INFER, upper_dir, chn)
        
        kst_start_time = args.start_time
        kst_end_time = args.end_time if args.end_time is not None else kst_start_time
        make_image(kst_start_time, kst_end_time, cfg, log, err_log)
    
    elif args.mode == "data":
        setproctitle("sat_{0}_data".format(chn))
        log, err_log = setting_log(cfg.RAWDATA, upper_dir, chn)
        data_extract(cfg, log, err_log)
    
    else:
        pass


if __name__ == "__main__":
    import gc
    
    # 순환참조(자기 자신 변수를 자기 자신 변수로 만드는 것) 없다 가정
    # gc.set_threshold(100, 0, 0)
    gc.disable()
    
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c", "--config", type=str,
        help="a config file in yaml format to control experiment"
        )
    parser.add_argument(
        "-m", "--mode", type=str,
        choices=["train", "test", "predict", "valid", "image", "data"]
        )
    parser.add_argument(
        "-st", "--start_time", type=str, default=None,
        help="start time used for prediction (KST) e.g. 202203150700"
        )
    parser.add_argument(
        "-et", "--end_time", type=str, default=None,
        help="end time used for prediction (KST) e.g. 202203150850"
        )

    args = parser.parse_args()

    main(args)
