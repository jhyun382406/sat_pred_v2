"""
위/경도 이용하는 open-elevation API 사용.
https://github.com/Jorl17/open-elevation/
사용법: https://github.com/Jorl17/open-elevation/blob/master/docs/api.md
"""
import json
import requests
import time
import logging
import numpy as np
import pandas as pd
from setproctitle import setproctitle


setproctitle("ko500_elev_api")

# 로그
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
stream_handler = logging.StreamHandler()
logger.addHandler(stream_handler)


def load_latlon(latlon_path):
    """
    Description:
        픽셀 별 위/경도 파일 불러오기.
        GK2A 공식 자료실에서 받은 파일.
        https://datasvc.nmsc.kma.go.kr/datasvc/html/base/cmm/selectPage.do?page=static.software&en=ko
    Args:
        latlon_path: str, latlon 텍스트 파일 경로
    Returns:
        latlon: numpy array, latlon 배열
    """
    return np.loadtxt(latlon_path, delimiter="\t", dtype=float)


def make_latlon_list(latlon):
    """
    Description:
        API에 맞는 꼴인 list로 latlon 변환
    Args:
        latlon: numpy array, latlon 배열
    Returns:
        latlon_list: list, 원소: {"latitude": float값, "longitude": float값}
    """
    latlon_list = []
    for elm in latlon:
        latlon_list.append({"latitude": elm[0], "longitude": elm[1]})
    return latlon_list


def elevation_api_post(i, api_url, latlon_list_sliced, API_SLEEP_SEC):
    """
    Description:
        API에 맞는 꼴인 list로 latlon 변환.
        한 번에 많은 양을 API로 보낼 수 없어 잘라내어 사용.
        200 이 아니라면 잠시 후 재시도.
    Args:
        i: int, 인덱스
        api_url: str, API URL
        latlon_list_sliced: list, 슬라이싱한 latlon 리스트
        API_SLEEP_SEC: int, 중간에 강제 쉬는 시간(초)
    Returns:
        result: dict, API post 결과
    """
    post_json = {"locations": latlon_list_sliced}
    post_request = requests.post(api_url, json=post_json)
    status = str(post_request.status_code)
    if status == "200":
        result = json.loads(post_request.text)
    else:
        logger.error(">>> Error on {0}".format(i))
        time.sleep(API_SLEEP_SEC)
        result = elevation_api_post(
            i, api_url, latlon_list_sliced, API_SLEEP_SEC
        )
    return result


def save_result(result_list, result_path):
    """
    Description:
        API 결과를 dataframe으로 변환 후 csv 저장.
    Args:
        result_list: list, API 결과를 list로 합친 것
        result_path: str, 결과 저장 경로
    Returns:
        None
    """
    result_df = pd.DataFrame(result_list)
    result_df.to_csv(result_path, encoding="utf-8", index=False)


if __name__ == "__main__":
    latlon_path = "./LATLON_KO_500.txt"
    result_path = "./LATLON_KO_500_ELEVATION_110.csv"
    api_url = "https://api.open-elevation.com/api/v1/lookup"
    SEND_LEN = 1000
    PRINT_FREQ = 10
    SAVE_FREQ = 1000
    API_SLEEP = 50
    API_SLEEP_SEC = 5
    
    latlon = load_latlon(latlon_path)
    latlon_list = make_latlon_list(latlon)
    
    # API 호출 loop
    result_list = []
    LOOP_NUM = len(latlon_list) // SEND_LEN
    logger.info(">>> Start")
    for i in range(LOOP_NUM):
        if i == 110:
            latlon_list_sliced = latlon_list[i*SEND_LEN:(i+1)*SEND_LEN]
            result = elevation_api_post(
                i, api_url, latlon_list_sliced, API_SLEEP_SEC
            )
            result_list.extend(result["results"])
            if i % PRINT_FREQ == 0 or i == LOOP_NUM-1:
                logger.info(">>>>> {0}".format(i))
            # 중간 저장
            if i % SAVE_FREQ == 0:
                save_result(result_list, result_path)
            # 적정 주기마다 쉬었다가 API 다시 호출
            if i % API_SLEEP:
                time.sleep(API_SLEEP_SEC)
        else:
            pass
    
    # 저장
    save_result(result_list, result_path)
    logger.info(">>> Complete")
    
