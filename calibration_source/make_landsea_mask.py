"""
위/경도 이용하는 global-land-mask 라이브러리 사용.
https://github.com/toddkarin/global-land-mask
"""
import numpy as np
import cv2
from global_land_mask import globe


def load_latlon(latlon_path):
    """
    Description:
        픽셀 별 위/경도 파일 불러오기.
        GK2A 공식 자료실에서 받은 파일.
        https://datasvc.nmsc.kma.go.kr/datasvc/html/base/cmm/selectPage.do?page=static.software&en=ko
    Args:
        latlon_path: str, latlon 텍스트 파일 경로
    Returns:
        latlon: numpy array, latlon 배열
    """
    return np.loadtxt(latlon_path, delimiter="\t", dtype=float)


def judge_land(latlon, h, w):
    """
    Description:
        픽셀 별 위/경도로 육지 여부 판단.
    Args:
        latlon: numpy array, latlon 배열
        h: int, 높이
        w: int, 너비
    Returns:
        land: numpy array, 육지면 True, 해상이면 False인 배열
    """
    _lat, _lon = latlon[::, 0], latlon[::, 1]
    land = globe.is_land(_lat, _lon)
    land = land.reshape(h, w)
    return land


def judge_ocean(latlon, h, w):
    """
    Description:
        픽셀 별 위/경도로 해상 여부 판단.
    Args:
        latlon: numpy array, latlon 배열
        h: int, 높이
        w: int, 너비
    Returns:
        ocean: numpy array, 해상이면 True, 육지면 False인 배열
    """
    _lat, _lon = latlon[::, 0], latlon[::, 1]
    ocean = globe.is_ocean(_lat, _lon)
    ocean = ocean.reshape(h, w)
    return ocean


def save_mask_img(mask, img_path):
    """
    Description:
        Mask를 이미지로 변경해서 저장.
    Args:
        mask: numpy array, 육지 또는 해상으로 True/False인 배열
        img_path: str, Mask 이미지 저장 경로
    Returns:
        None
    """
    mask = mask.astype(np.uint8) * 255
    cv2.imwrite(img_path, mask)


if __name__ == "__main__":
    latlon_path = "./LATLON_KO_2000.txt"
    img_path = "./landsea_2000.png"
    h, w = 900, 900
    
    latlon = load_latlon(latlon_path)
    land = judge_land(latlon, h, w)
    save_mask_img(land, img_path)
