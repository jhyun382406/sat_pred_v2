from flask import Flask, request
from flask_restx import Resource, Api
from flask_cors import CORS

import json
import datetime
import logging
import logging.config
from pytz import timezone

from utils.code_utils import convert
from main import setting_log
from src.openstl_preprocess import SatPrep
from src.openstl_model import OpenSTLModel
from src.openstl_inference import SatInfer


# Global vars.
CFG_FILE = "./configs/openstl_sw038_cfg.yaml"
API_ADDR = "/sat_pred"

##################################################################
class CustomFormatter(logging.Formatter):
    """
    Description:
        KST 시간대를 사용자 정의 포맷으로 변경.
    """
    def converter(self, timestamp):
        """
        Description:
            KST로 시간대 변경.
        Args:
            timestamp: timestamp, UTC 기준 시간대
        Returns:
            dt: timestamp, KST로 변경한 시간대
        """
        dt = datetime.datetime.fromtimestamp(timestamp)
        return dt.astimezone(timezone("Asia/Seoul"))

    def formatTime(self, record, datefmt=None):
        """
        Description:
            사용자 정의 포맷으로 시간대 변경.
        Args:
            record: record, UTC 기준 시간대 객체
            datefmt: str, 문자열로 시간대를 변경시킬 포맷 (default=None)
        Returns:
            s: str, 사용자 정의 포맷으로 변경한 시간대
        """
        dt = self.converter(record.created)
        if datefmt:
            s = dt.strftime(datefmt)
        else:
            try:
                s = dt.isoformat(timespec="milliseconds")
            except TypeError:
                s = dt.isoformat()
        return s

log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": CustomFormatter,
            "format": "[%(levelname)s][%(filename)s:%(lineno)s][%(asctime)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "default",
            "level": "DEBUG"
        }
    },
    "root": {"handlers": ["console"], "level": "WARNING"},   # 최상위 logger에 handler 연결
    "loggers": {
        "infolog": {"handlers": ["console"], "level": "INFO", "propagate": False},
        "infolog.debug": {"handlers": ["console"], "level": "DEBUG", "propagate": False},
        "infolog.error": {"handlers": ["console"], "level": "ERROR", "propagate": False}
    },   # 특정 모듈 logger
}
logging.config.dictConfig(log_config)
log = logging.getLogger("infolog")
deb = logging.getLogger("infolog.debug")
err = logging.getLogger("infolog.error")
##################################################################

# Load pytorch model
cfg = convert(CFG_FILE)
prep = SatPrep(cfg, log, err, pred_mode=True)
model = OpenSTLModel(cfg, log, err)
infer = SatInfer(cfg, log, err, prep)
openstl_args = model("test")
openstl = infer.make_baseinstance(openstl_args)
openstl = infer.load_trained(openstl, infer.load_model_dir)
log.info(">>> Ready to inference")

app = Flask(__name__)
api = Api(app)

# API 호출 시 CORS 정책 오류 해결을 위해 넣음
# resources에 CORS 정책 허용해 줄 API 주소를 넣으면 됨
CORS(app, resources=r"{0}".format(API_ADDR))

# api.route("호출 시 사용할 API 주소")
@api.route(API_ADDR)
class InferApi(Resource):
    """
    추론 API.
    전역변수로 불러온 모델로부터 추론 진행 후 결과 산출.
    get, post, put, patch, delete 함수로 오버라이드
        - GET: 리소스 조회 (최근에는 Representation이라는 이름을 많이 사용한다.)
        - POST: 요청 데이터 처리, 주로 등록에 사용
        - PUT: 리소스를 대체, 해당 리소스가 없으면 생성
        - PATCH: 리소스 부분 변경 (잘 안되면 POST로 사용)
        - DELETE: 리소스 삭제
    """
    def post(self):
        # API로 request 온 데이터를 받는 부분
        # post 형태의 호출이라 body에 json 형태로 담아서 호출함
        log.info(">>> input data: {0} / type: {1}".format(request.json, type(request.json)))
        kst_start_time = str(request.json.get("kst_start_time"))
        kst_end_time = request.json.get("kst_end_time")
        if kst_end_time is not None:
            kst_end_time = str(kst_end_time)
        else:
            kst_end_time = kst_start_time
        
        # return으로 보내 줄 response dict 선언
        response_dict = {}
        
        # 추론
        infer_dl, utc_time_list = prep(kst_start_time, kst_end_time)
        infer(openstl_args, openstl, infer_dl, utc_time_list)
        
        chn = cfg.PARAMS.DATA.CHANNEL.upper()
        kst_s_t = self._arrange_ymdhm(kst_start_time)
        kst_e_t = self._arrange_ymdhm(kst_end_time)
        msgs = [
            "",
            "Save npy files on prediction results storage.",
            "File naming is written by UTC time (KST-9)."
        ]
        if kst_e_t is None:
            msg = "Inference GK2A {0} on {1} (KST) completely."
            msgs[0] = msg.format(chn, kst_s_t)
        else:
            msg = "Inference GK2A {0} on {1} ~ {2} (KST) completely."
            msgs[0] = msg.format(chn, kst_s_t, kst_e_t)
        response_dict["msg"] = " ".join(msgs)
        
        # response dict를 이용해서 response 객체 생성
        response = app.response_class(
            response=json.dumps(response_dict),
            status=200,
            mimetype="application/json"
        )
        
        return response
    
    def _arrange_ymdhm(self, t):
        """
        Description:
            12자리 연월일시분을 보기 좋게 변경.
            202312311200 -> 2023.12.31. 12:00
        Args:
            t: str, 12자리 연월일시분
        Returns:
            cvt_t: str, 연월일시분 정리
        """
        if len(t) == 12 and isinstance(t, str):
            y, m, d, h, mi = t[:4], t[4:6], t[6:8], t[8:10], t[10:]
            return "{0}.{1}.{2}. {3}:{4}".format(y, m, d, h, mi)
        else:
            return t


if __name__ == "__main__":
    # 멀티스레딩보다 멀티프로세싱이 더 좋음 (torch는 멀티스레딩 불가)
    app.run(debug=True, host="0.0.0.0", port=5000, threaded=False)
