FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime

RUN apt-get update && apt-get install -y vim sudo libgl1-mesa-glx libglib2.0-0

# USER 추가 및 sudo 권한 설정 (CHPASSWD는 계정:비밀번호 구조)
ARG USER="wizai" \
  CHPASSWD="wizai:wizai"
RUN adduser --disabled-password --gecos "" $USER \
  && echo $CHPASSWD | chpasswd \
  && adduser $USER sudo \
  && echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
# Basis 이미지의 /root에서 .bashrc 복사 후 USER 변경 (선택)
  && cat /root/.bashrc >> /home/$USER/.bashrc \
  && chown $USER:$USER /home/$USER/.bashrc

# pip PATH 지정 안되어 추가
RUN python -m pip install --upgrade pip \
  && pip install dask==2022.10.2 \
  decord==0.6.0 \
  fvcore==0.1.5.post20221221 \
  hickle==5.0.2 \
  lpips==0.1.4 \
  matplotlib==3.6.2 \
  netCDF4==1.6.2 \
  numpy==1.23.4 \
  opencv-python==4.7.0.68 \
  packaging==21.3 \
  pandas==1.5.2 \
  scikit-learn==1.1.3 \
  scikit-image==0.19.3 \
  timm==0.6.11 \
  tqdm==4.63.2 \
  xarray==0.19.0 \
  pysolar==0.11 \
  setproctitle==1.3.2 \
  && pip install -U "ray==2.3.1"

USER $USER
ENV PATH=/home/$USER/.local/bin:$PATH

# key value 식으로 이미지에 코멘트 작성
LABEL title="OpenSTL" \
  version="231109" \
  baseimage="pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime" \
  description="Satellite video prediction using OpenSTL"
