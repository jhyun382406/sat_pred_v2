# Predict Satelite(GK2A) Images Sequence

[[_TOC_]]

## Name
- GK2A 위성영상 예측
- 2023년도 ['위성영상예측_2023'](https://gitlab.com/jhyun382406/sat_pred)에 이어서 진행
- Pytorch 기반 [OpenSTL](https://github.com/chengtan9907/OpenSTL/) 사용, 기본 U-net 구조에 latent 부분에 시공간 예측 부분 포함
  - 최초 도입한 방법(Unet+ConvLSTM)과 아이디어 동일
  - v0.3.0을 수정하여 이용 중 (24년 4월 기준 v1.0.0 이 있어 추후 변경 필요)

## Description
- 기본적으로 위성 영상(NetCDF 파일, 2차원 이미지 형식)을 이용하여 다음 위성 영상을 예측
- 물리자원 고려하여 900x900 (해상도 2km), 3600x3600 (해상도 500m) 데이터 조정하여 사용
- 위/경도 및 Calibration table은 [기상위성센터](http://datasvc.nmsc.kma.go.kr/datasvc/html/base/cmm/selectPage.do?page=static.software&lang=ko)에서 받을 수 있음
  - 위/경도: LCC 도법, 한반도 영역
  - Calibration table: 필요한 것만 CSV 파일로 만들어 줘야 함
- 태양천정각(SZA)을 고려하여 주간, 야간, 여명/황혼 시간대 판단 후 학습 데이터에 사용
  - 해당 코드에서는 동쪽과 서쪽 대표 픽셀로 해당 영상의 시간대 판단
- OpenSTL Model 버전 설명
  - config 파일에서 모두 설정
  - TAU 이용
  - 전처리
    - extract: Calibration 한 결과를 저장하는 식으로 변경
    - Satprep: **RAW값** 그대로 이용
    - SatprepV2: ~~Calibration 후~~ **변화량(delta)** 이용

## Environment
- ~~Base 도커 이미지: pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime~~
- ~~우분투 버전: Ubuntu 20.04.6 LTS~~
- NHN클라우드 전용풀 VM
  - Ubuntu 22.04.3 LTS
  - Intel(R) Xeon(R) Gold 6448Y (16 core) / RAM 224GB / NVIDIA H100 80GB HBM3
  - nvidia-driver: 535.104.12-0ubuntu1 / CUDA 12.2

|사용 라이브러리|버전|
|:---:|---:|
|Python|3.10.13|
|torch|2.0.1|
|numpy|1.23.4|
|pandas|1.5.2|
|opencv-python|4.7.0.68|
|matplotlib|3.6.2|
|dask|2022.10.2|
|netCDF4|1.6.2|
|pysolar|0.11|
|ray|2.3.1|

- 이외 파이썬 라이브러리: requirements.txt
- 전체 컨테이너 구성 환경: dockerfile/openstl.dockerfile 참고
- ray 라이브러리는 docker container 생성 시 shm-size 옵션을 추가 부여해주어야 원활히 작동됨

## Execution
main.py 에서 config/ 에 있는 yaml 파일을 반영하여 실행  
- 데이터 파싱  
```python
python main.py -c /path/of/config/file -m data
```
- 학습  
```python
python main.py -c /path/of/config/file -m train
```
- 추론  
```python
python main.py -c /path/of/config/file -m predict -st 202203150700 -et 202203150850
```
- 이미지 생성  
```python
python main.py -c /path/of/config/file -m image -st 202203150700 -et 202203150850
```

## File Structure
```bash
├── openstl/   # OpenSTL 메인 코드
│   ├── api/
│   │    └── train.py   # BaseExperiment 클래스가 모델의 가장 기본 베이스
│   ├── methods/   # 알고리즘 별 핵심 엔진 부분, loss 바꿀 때 확인
│   ├── models/   # 모델 구조, arguments 확인 가능
│   ├── modules/   # 모델 별 필요한 layer 설정
│   └── utils/
│        └── parser.py   # config 파일에 추가할 내용들 확인 가능 (일부는 공식 코드 configs에만 있기도 함)
├── tools/   # OpenSTL 에서 추가 사용하는 코드 및 메인 함수 존재
├── src/
│   ├── custom_loss.py   # Loss 함수, 원래 loss 함수에 증폭값 부여 (현재 미사용)
│   ├── data_parser.py   # Normalizer 등 데이터 변환
│   ├── extract_data.py   # 학습 및 추론 시 원본 데이터를 array로 가져옴
│   ├── openstl_dataloader.py   # OpenSTL에 적용할 Dataloader
│   ├── openstl_dataset.py   # pytorch Dataset으로 만든 1개 데이터 추출용
│   ├── openstl_make_img.py   # 추론 결과 및 해당 시간대 원본 위성 흑백 이미지 생성 및 저장
│   ├── openstl_model.py   # OpenSTL 모델 및 학습 관련 설정
│   ├── openstl_preprocess.py   # 입력 데이터 전처리 + dataloader 생성
│   ├── openstl_train.py   # OpenSTL 인스턴스 생성 및 모델 학습
│   ├── openstl_valid.py   # 모델 검증 및 검증 결과 저장
│   └── sat_calibration.py   # 위성 데이터 계산식 적용
├── utils/
│   ├── code_utils.py   # 범용 유틸리티 함수
│   ├── log_module.py   # 로그 용
│   ├── tf_settings.py   # tensorflow 설정
│   └── torch_settings.py   # pytorch 설정
├── configs/   # 채널 별로 설정 yaml 파일 적재
├── calibration_source/   # Calibration에 사용할 데이터
│   ├── make_landsea_mask.py   # global-land-mask 라이브러리 이용하여 위/경도를 Landsea mask 처리한 데이터 산출
│   └── make_elevation.py   # open-elevation API 이용하여 위/경도로 고도 구한 뒤 저장
├── test_code/   # test용으로 메인으로 꺼내와서 사용하는 코드
│   ├── test_cvt_dat.py   # dat을 zarr나 npy_stack으로 변환하여 저장
│   └── test_dl.py   # dat, zarr, npy_stack의 torch dataloader 성능 테스트
├── dockerfile/
├── log/
├── err_log/
├── openstl_model_[채널명]/   # 각 채널별 학습 모델, 설정
├── requirements.txt   # 파이썬 라이브러리
├── api_inference.py   # Flask + Gunicorn 이용한 모델 추론 API
└── main.py   # 전체 통합 메인 모듈 (데이터 파싱, 학습, 추론, 이미지 생성, 검증)
```  

## Scheduled Tasks
- 학습 중 NaN 경우 처리
  - base_method에서 검증 및 metrics도 nan 부분 처리 필요
  - pred_y 쪽 NaN 발생하는 경우 찾아야 함
- 다른 채널도 512 crop 버전 및 원본 버전(또는 900) 분리하여 테스트
- 추론 결과 생성
  - delta 값이므로 Input Sequence로부터 계산식 적용 필요

## Current Issues
- 학습 중 NaN 경우 처리
  - input에서 NaN 발생되는 경우, delta 값이므로 0으로 치환
  - clip_grad 도 설정한 상태
  - method 쪽에서 loss_scaler 설정하면 학습 중 pred_y에서 NaN 발생하는 경우 발생
- Dataloader 효율성
  - torch dataloader와 dask compute의 num_workers 조절해야 정상 작동 확인
  - dataloader 쪽 workers를 0으로 했을 때가 더 빠른 상태

## Additional Research
- Loss function 바꿔서 진행해보기
  - 이미지 전체가 아닌 일부 patch에서만 loss 구하는 거 적용
  - Super Resolution에서 사용하는 Perceptual loss 내용 반영해보기 (선명도 증대)
- 네트워크 리서치
  - Graphcast(Google Deepmind): graph 형식 데이터로 변환하는게 문제
  - torch-harmonics(Nvidia): 코드 예제 및 내용 이해 필요