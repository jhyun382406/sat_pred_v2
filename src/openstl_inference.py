import os
import time
import numpy as np
import torch

from openstl.api import BaseExperiment
from openstl.utils import setup_multi_processes
from src.extract_data import Data
from src.sat_calibration import SatCalibration
from src.data_parser import min_max_normalizer, inv_min_max_normalizer
from utils.code_utils import return_dir, get_arguments, combined_indices_of_cropped
from utils.log_module import print_elapsed_time


class SatInfer(object):
    """Inference Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log, prep):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 추론 init 설정.
            모델 추론 결과 산출 및 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
            prep: SatPrep, 전처리 인스턴스
        """
        super(SatInfer, self).__init__()
        cfg_pr = cfg.PARAMS
        cfg_r = cfg.RAWDATA
        cfg_i = cfg.INFER
        
        self.load_model_dir = cfg_i.LOAD_MODEL_DIR
        self.chn = cfg_pr.DATA.CHANNEL
        self.output_seq = cfg_pr.DATA.OUTPUT_SEQ
        self.norm = cfg_pr.NORMALIZER
        self.input_shape = cfg_pr.DATA.INPUT_SHAPE
        self.time_step = cfg_r.TIME_STEP
        self.dtype = cfg_r.DATA_TYPE
        self.infer_loop = cfg_i.LOOP
        self.batch_size = cfg_i.BATCH_SIZE
        self.infer_saved_dir = cfg_i.OUTPUT_DIR
        self.infer_saved_file = cfg_i.OUTPUT_FILE
        
        self.time_data = Data()
        self.calib = SatCalibration(cfg, log, err_log)
        self.prep = prep
        
        self.log, self.err_log = log, err_log

    def __call__(self, args, openstl, infer_dl, utc_time_list):
        """
        Description:
            모델 추론 후 결과는 npy로 저장.
            Calibration table은 미적용 상태.
        Args:
            args: namedtuple, OpenSTL 설정
            openstl: openstl.api.BaseExperiment, OpenSTL 인스턴스
            infer_dl, torch.Dataloader 객체, Dataloader 역할(추론)
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        Returns:
            None
        """
        self.log.info(">>> Start Inference")
        start_time = time.time()
        
        # set multi-process settings
        setup_multi_processes(args.__dict__)
        
        # 추론 결과 생성
        results = self.inference(openstl, infer_dl)
        results_reshape = self.restore_origin(results, utc_time_list)
        
        # 저장
        self.save_results(results_reshape, utc_time_list)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End Inference")
    
    def make_baseinstance(self, args):
        """
        Description:
            OpenSTL 인스턴스 생성.
        Args:
            args: namedtuple, OpenSTL 설정
        Returns:
            openstl: openstl.api.BaseExperiment, OpenSTL 인스턴스
        """
        dummy_dl = self._make_dummy_dl()
        openstl = BaseExperiment(
            args,
            dataloaders=(dummy_dl, dummy_dl, dummy_dl)
        )
        return openstl
    
    def _make_dummy_dl(self):
        """
        Description:
            OpenSTL 모델 불러오기 위한 더미 Dataloader 만들기.
            해당 Dataloader는 실제 추론에서 사용되지 않음.
        Args:
            None
        Returns:
            dummy_dl, torch.Dataloader 객체, 추론 모델 불러오는 임시용
        """
        from torch.utils.data import (
            DataLoader, BatchSampler, SequentialSampler
        )
        from src.openstl_dataset import GK2ADatasetV3
        
        # 임시 데이터
        memory_map = np.zeros(1,)
        seq_indices = [0]
        # 원래 Dataloader 형상에 맞추어 생성
        ds = GK2ADatasetV3(memory_map=memory_map, seq_indices=seq_indices)
        batch_sampler = BatchSampler(
            SequentialSampler(ds),
            batch_size=1,
            drop_last=False
        )
        dummy_dl = DataLoader(
            ds,
            batch_sampler=batch_sampler,
            pin_memory=True
        )
        return dummy_dl
    
    def load_trained(self, openstl, load_model_dir):
        """
        Description:
            모델 불러오기.
        Args:
            openstl: openstl.api.BaseExperiment, OpenSTL 인스턴스
            load_model_dir: str, 불러올 모델 디렉토리
        Returns:
            openstl: openstl.api.BaseExperiment, OpenSTL 인스턴스
        """
        load_model_path = os.path.join(
            load_model_dir, "checkpoint.pth"
        )
        openstl._load_from_state_dict(torch.load(load_model_path))
        self.log.info(">>> Setting OpenSTL instance & Load model's state_dict")
        return openstl
    
    def inference(self, openstl, infer_dl):
        """
        Description:
            OpenSTL 인스턴스 생성 및 모델 불러오기.
        Args:
            openstl: openstl.api.BaseExperiment, OpenSTL 인스턴스
            infer_dl, torch.Dataloader 객체, Dataloader 역할(추론)
        Returns:
            results: numpy array, 추론 결과 배열
                - (batch_total_length, seq_len, channel, height, width)
        """
        results = []
        for batch_x in infer_dl:
            with torch.no_grad():
                # batch_x, batch_y = batch_x.to(self.device), batch_y.to(self.device)
                batch_x = batch_x.cuda()
                pred_y = openstl.method._predict(batch_x, None)   # batch_y = None
                if openstl.args.empty_cache:
                    torch.cuda.empty_cache()
                results.append(pred_y)
        results = torch.cat(results, dim=0).cpu().numpy()
        self.log.info(">>> Results: {0}".format(results.shape))
        return results
    
    def restore_origin(self, results, utc_time_list):
        """
        Description:
            추론 결과를 원래 위성영상 값, 형상으로 복원.
            Inverse normalize는 기존 평균/표준편차 형상에 맞추어 먼저 진행.
        Args:
            results: numpy array, 추론 결과 배열
                - (batch_total_length, seq_len, channel, height, width)
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        Returns:
            results_reshape: numpy array, 유효 영역으로 복원한 추론 결과 데이터
                - (data_len, seq_len, height, width, channel)
        """
        # Calibration table & Latitude/Longitude array
        calibration_table = self.calib.make_calibration_table()
        latlon = self.calib.load_latlon()
        effective_area_pixel = self.calib.make_effective_area_pixel_outline(latlon)
        
        # Restore value
        sat_mean, sat_std = self.prep.load_mean_std(self.prep.load_model_dir)
        inv_normalizer = self.prep.get_normalizer(self.norm.INV_FUNCTION,
                                                  calibration_table,
                                                  sat_mean,
                                                  sat_std)
        results = inv_normalizer(results)
        results = results.transpose(0, 1, 3, 4, 2)
        self.log.info(">>> Inverse normalize & channel first -> last")
        
        # Restore Shape
        results_reshape = self.restore_effective_area(results,
                                                      utc_time_list,
                                                      effective_area_pixel)
        # 값 범위 및 데이터 타입 맞추기
        results_reshape = self.arrange_data(results_reshape)
        self.log.info(">>> Arrange original area & data type")
        self.log.info(">>> Result Shape: {0}".format(results_reshape.shape))
        
        return results_reshape
    
    def restore_effective_area(self,
                               result_concat,
                               utc_time_list,
                               effective_area_pixel):
        """
        Description:
            추론 결과를 input_shape 대로 crop 전의 유효 영역으로 복원.
        Args:
            result_concat: numpy array, 추론 결과 데이터
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Return:
            results_reshape: numpy array, 유효 영역으로 복원한 추론 결과 데이터
                - (data_len, seq_len, height, width, channel)
        """
        lat_pix_min, lat_pix_max, lon_pix_min, lon_pix_max = effective_area_pixel
        data_h, data_w = lat_pix_max - lat_pix_min, lon_pix_max - lon_pix_min
        input_h, input_w, _ = self.input_shape

        # 가로세로 몇 개 나눌지
        h_start_indices, w_start_indices = combined_indices_of_cropped((1, data_h, data_w), self.input_shape)
        h_num = len(h_start_indices)
        w_num = len(w_start_indices)
        
        results_reshape_shape = (len(utc_time_list), self.output_seq, data_h, data_w, 1)
        results_reshape = np.empty(results_reshape_shape, dtype=np.float32)
        results_reshape_divide = np.zeros_like(results_reshape)
        adapt_indices_range = np.arange(result_concat.shape[0], step=h_num*w_num)
        
        # Dataloader에서 crop한대로 복원
        CHECKNUM = 0
        for h_i in h_start_indices:
            for w_i in w_start_indices:
                adapt_indices = adapt_indices_range + CHECKNUM
                results_reshape[:, :, h_i:h_i+input_h, w_i:w_i+input_w, :] += result_concat[adapt_indices, ...]
                results_reshape_divide[:, :, h_i:h_i+input_h, w_i:w_i+input_w, :] += 1
                CHECKNUM += 1
        
        # 중복해서 더해진 부분 나누기
        results_reshape = np.divide(results_reshape, results_reshape_divide)

        return results_reshape
    
    def arrange_data(self, results_reshape):
        """
        Description:
            원래 NetCDF 때의 값 범위, 데이터 타입으로 변경.
        Args:
            results_reshape: numpy array, 유효 영역으로 복원한 추론 결과 데이터
        Returns:
            results_reshape: numpy array, 처리 완료된 추론 결과 데이터
        """
        _min = getattr(self.norm, self.chn.upper()).MIN_VALUE
        _max = getattr(self.norm, self.chn.upper()).MAX_VALUE
        results_reshape = np.round(results_reshape)
        results_reshape = results_reshape.clip(_min, _max)
        results_reshape = results_reshape.astype(getattr(np, self.dtype))
        return results_reshape

    def save_results(self, result_concat, utc_time_list):
        """
        Description:
            모델 결과 npy로 저장.
            np.save로 저장하여 np.load로 불러와야 함.
            저장은 ~~~/ticket_time/pred_time_01 ~ pred_time_xx
        Args:
            result_concat: numpy array, 추론 결과 데이터
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        Returns:
            None
        """
        # 입력 UTC 시간대로 폴더 생성
        for idx, utc_time in enumerate(utc_time_list):
            saved_dir = return_dir(True,
                                   self.infer_saved_dir,
                                   self.chn,
                                   utc_time[:4],   # year
                                   utc_time[4:6],   # month
                                   utc_time[6:8],   # day
                                   utc_time)
            one_preds = result_concat[idx]
            try:
                # 각각 UTC 시간대로 폴더마다 OUTPUT_SEQ * LOOP 개수만큼 결과 저장
                self._save_result_npy(one_preds, utc_time, saved_dir)
            except Exception as e:
                self.err_log.error(">>> Error on save_results in {0}(UTC)".format(utc_time))
                self.err_log.error(">>> Error on save_results in {0}(UTC): {1}".format(utc_time, e))
            else:
                self.log.info(">>> Save {0}(UTC) results successfully".format(utc_time))
    
    def _save_result_npy(self, one_preds, utc_time, saved_dir):
        """
        Description:
            모델 결과 UTC_TIME 별 폴더에 개별 npy로 저장.
        Args:
            one_preds: numpy array, 1개 UTC_TIME에 대한 추론 결과 데이터
                - Shape: (output_seq_len * loop, height, width, 1)
            utc_time: str, UTC 연월일시분(12자리)
            saved_dir: 해당 UTC_TIME의 추론 결과 저장 디렉토리
        Returns:
            None
        """
        for idx in range(self.output_seq * self.infer_loop):
            one_pred = one_preds[idx]
            utc_time = self.time_data._compute_date(utc_time, "min", self.time_step)
            save_file = self.infer_saved_file.format(self.chn, utc_time)
            save_path = os.path.join(saved_dir, save_file)
            np.save(save_path, one_pred)