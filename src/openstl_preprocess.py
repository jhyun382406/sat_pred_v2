import os
import re
from datetime import timezone, timedelta, datetime
from itertools import chain
from functools import partial
import numpy as np
import dask.array as da
from torch.utils.data import (
    DataLoader, RandomSampler, BatchSampler, SequentialSampler
)

from utils.code_utils import get_arguments
from src.extract_data import GK2ADataExtractor
from src.sat_calibration import SatCalibration
from src.openstl_dataset import GK2ADatasetV3, GK2ADatasetV4
from src.openstl_dataloader import SatCollator, worker_init
from src.data_parser import *


class SatPrep(object):
    """Preprocess using Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log, pred_mode=False):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 전처리 init 설정.
            학습: Model에 fit시킬 Dataloader 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
            pred_mode: bool, 추론 모드 판단
        """
        super(SatPrep, self).__init__()
        # Data
        cfg_d = cfg.PARAMS.DATA
        cfg_tr = cfg.TRAIN
        cfg_i = cfg.INFER
        cfg_t = cfg.TEST
        self.cfg_params = cfg.PARAMS
        self.pred_mode = pred_mode
        self.chn = cfg_d.CHANNEL
        self.in_seq = cfg_d.INPUT_SEQ
        self.out_seq = cfg_d.OUTPUT_SEQ
        self.fill_method = cfg_d.FILL.METHOD
        self.fill_limit = cfg_d.FILL.LIMIT
        if not pred_mode:
            self.total_seq = cfg_d.INPUT_SEQ + cfg_d.OUTPUT_SEQ
        else:
            self.total_seq = cfg_d.INPUT_SEQ
        
        # Training preprocess
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.gaussian_mean_file = self.cfg_norm.MEAN_FILE.format(self.chn)
        self.gaussian_std_file = self.cfg_norm.STD_FILE.format(self.chn)
        self.minmax_min_file = self.cfg_norm.MIN_FILE.format(self.chn)
        self.minmax_max_file = self.cfg_norm.MAX_FILE.format(self.chn)
        self.dat_dir = cfg_tr.DATA_DIR
        self.dat_names = [d.format(self.chn) for d in cfg_tr.DATA_FILE]
        self.shape = cfg_tr.DATA_SHAPE
        self.valid_dat_names = [d.format(self.chn) for d in cfg_tr.VALID_DATA_FILE]
        self.valid_shape = cfg_tr.VALID_DATA_SHAPE
        self.dtype = getattr(np, cfg_tr.DATA_TYPE)
        self.chunk_num = cfg_tr.DASK_CHUNK
        self.batch_size = cfg_tr.BATCH_SIZE
        self.dl_num_workers = cfg.DATALOADER.NUM_WORKERS
        self.dl_pin_mem = cfg.DATALOADER.PIN_MEMORY
        self.dask_workers = cfg.DATALOADER.DASK_WORKERS
        
        # zarr, npy configs
        self.saved_method = cfg_tr.SAVED_METHOD
        self.grp_sat = cfg_tr.ZARR_GROUP.SAT

        # Inference preprocessing
        self.test_batch_size = cfg_t.BATCH_SIZE
        self.infer_batch_size = cfg_i.BATCH_SIZE
        self.time_step = cfg.RAWDATA.TIME_STEP
        self.load_model_dir = cfg_i.LOAD_MODEL_DIR
        self.sat = GK2ADataExtractor(cfg, log, err_log, pred_mode)
        
        # Additional
        self.CVT_TIME_FORMAT = cfg.RAWDATA.CVT_TIME_FORMAT
        self.TZ_KST = timezone(timedelta(hours=9))
        self.calib = SatCalibration(cfg, log, err_log)
        self.crop_method = cfg_d.CROP.METHOD
        self.input_shape = cfg_d.INPUT_SHAPE

        self.log, self.err_log = log, err_log
        
    def __call__(self, kst_start_time=None, kst_end_time=None):
        """
        Description:
            학습, 추론 전처리.
            GK2A Calibration table 사용 분기 및 태양천정각 사용 분기 적용.
                - GK2A Calibration table: 위성 raw 값 말고 radiance, albedo, brightness temperature 등으로 변환.
                - 태양천정각: 주간, 야간, 여명/황혼 분리.
        Args:
            학습 전처리
                None
            추론 전처리
                - kst_start_time: str, 추론할 기준 시작시점(연월일시분 12자리)
                - kst_end_time: str, 추론할 기준 최종시점(연월일시분 12자리)
        Returns:
            학습 전처리
                - train_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - valid_dl: torch.Dataloader 객체, Dataloader 역할(검증)
                - mean_std: tuple, (채널값 평균 (T, H, W), 채널값 표준편차 (T, H, W))
            추론 전처리
                - infer_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        """
        # Calibration table
        calibration_table = self.calib.make_calibration_table()
        latlon = self.calib.load_latlon()
        
        # Branch Training mode or Prediction mode
        if not self.pred_mode:
            train_dl, valid_dl, mean_std = self.train_prep(calibration_table,
                                                           latlon)
            return train_dl, valid_dl, mean_std
        else:
            infer_datas = self.get_infer_datas(kst_start_time,
                                               kst_end_time)
            kst_time_list = self.sat._make_time_list(kst_start_time,
                                                     kst_end_time,
                                                     "min",
                                                     self.time_step)
            utc_time_list = [self.sat._kst_to_utc(t) for t in kst_time_list]
            infer_dl = self.infer_prep(infer_datas,
                                       utc_time_list,
                                       calibration_table)
            return infer_dl, utc_time_list
    
    def train_prep(self, calibration_table, latlon):
        """
        Description:
            학습 전처리.
            모델에 사용할 dataloader 생성.
                - dat 파일 불러오기
                - 파일이 없어 결측 처리 안된 부분을 전후 데이터로 결측 처리
                - 연속적인 시간대 Sequence의 index를 추출
                - dataloader 생성
            평균, 표준편차는 저장을 위해 반환.
        Args:
            calibration_table: numpy array, 채널의 Calibration table
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            train_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - Input Shape: (batch_size, input_seq_len, 1, height, width)
                - Output Shape: (batch_size, output_seq_len, 1, height, width)
            valid_dl: torch.Dataloader 객체, Dataloader 역할(검증)
                - Input Shape: (batch_size, input_seq_len, 1, height, width)
                - Output Shape: (batch_size, output_seq_len, 1, height, width)
            sat_mean: numpy array, 채널값 평균 (T, H, W), (default: None)
            sat_std: numpy array, 채널값 표준편차 (T, H, W), (default: None)
        """
        train_datas = self.get_train_datas("Train",
                                           self.dat_names,
                                           self.shape,
                                           self.saved_method)
        train_datas, train_nonfill_indices = self.fill_zero_frame(train_datas)
        valid_datas = self.get_train_datas("Valid",
                                           self.valid_dat_names,
                                           self.valid_shape,
                                           self.saved_method)
        valid_datas, valid_nonfill_indices = self.fill_zero_frame(valid_datas)
        
        # SZA 사용여부는 make_trainable_indices 에서 처리
        train_kst_obj_list = self.get_file_times(self.dat_names)
        valid_kst_obj_list = self.get_file_times(self.valid_dat_names)
        file_exclude_seq_indices = self.make_file_exclude_seq_indices(self.dat_names, self.shape)
        train_seq_indices, valid_seq_indices = self.make_trainable_indices(latlon,
                                                                           train_kst_obj_list,
                                                                           valid_kst_obj_list,
                                                                           train_nonfill_indices,
                                                                           valid_nonfill_indices,
                                                                           file_exclude_seq_indices)
        # Normalizer
        normalizer_name = self.cfg_norm.FUNCTION
        sat_mean, sat_std = None, None
        if normalizer_name is None:
            normalizer = None
        elif normalizer_name.startswith("gaussian"):
            sat_mean, sat_std = self.compute_mean_std(train_datas, train_seq_indices)
            normalizer = self.get_normalizer(normalizer_name,
                                             calibration_table,
                                             sat_mean,
                                             sat_std)
        else:
            normalizer = self.get_normalizer(normalizer_name,
                                             calibration_table)
        
        # Make dataset
        train_dataset = GK2ADatasetV3(
            memory_map=train_datas,
            in_seq=self.in_seq,
            out_seq=self.out_seq,
            crop_method=self.crop_method,
            input_shape=self.input_shape,
            normalizer=normalizer,
            calibration_table=calibration_table,
            seq_indices=train_seq_indices,
            mean=sat_mean,
            std=sat_std,
            pred_mode=self.pred_mode
        )
        valid_dataset = GK2ADatasetV3(
            memory_map=valid_datas,
            in_seq=self.in_seq,
            out_seq=self.out_seq,
            crop_method=self.crop_method,
            input_shape=self.input_shape,
            normalizer=normalizer,
            calibration_table=calibration_table,
            seq_indices=valid_seq_indices,
            mean=sat_mean,
            std=sat_std,
            pred_mode=self.pred_mode
        )
        self.log.info("Using Train Dataset nums: {0}".format(len(train_dataset)))
        self.log.info("Using Valid Dataset nums: {0}".format(len(valid_dataset)))
        
        # Make dataloader
        train_dl = self.make_openstl_dl(train_dataset, self.batch_size)
        valid_dl = self.make_openstl_dl(valid_dataset, self.infer_batch_size)
        
        return train_dl, valid_dl, (sat_mean, sat_std)
    
    def get_train_datas(self,
                        kinds,
                        data_names,
                        shape,
                        method="dat"):
        """
        Description:
            학습용, 검증용 데이터 불러오기.
                - dat, zarr, npy 방식 구분
        Args:
            kinds: str, 학습/검증 구분
            dat_names: list, 데이터 파일명(KST)
            shape: list, 데이터 shape
            method: str, 사용할 데이터 (default: dat)
                - dat, zarr, npy
        Return:
            datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
                - Shape: (Data nums, height, width)
        """
        self.log.info("Load {0} datas".format(kinds))
        data_paths = [
            os.path.join(self.dat_dir, dat_name)
            for dat_name in data_names
        ]
        if method == "dat":
            datas = self.load_dat_dask(data_paths, shape)
        elif method == "zarr":
            datas = self.load_zarr_dask(data_paths, self.grp_sat)
        elif method == "npy":
            datas = self.load_npy_dask(data_paths)
        else:
            self.err_log.error("Write correct method (dat, zarr, npy): {0}".format(method))
        self.log.info("{0} data shape: {1}".format(kinds, datas.shape))
        return datas
    
    def load_dat_dask(self, data_paths, data_shape):
        """
        Description:
            저장한 dat files을 dask로 불러오기.
        Args:
            data_paths: list, 불러올 파일 리스트
            data_shape: list, 불러올 파일의 shape 리스트
        Returns:
            datas: dask array, 위성 dask 배열
        """
        memmap_list = []
        for data_path, sp in zip(data_paths, data_shape):
            data_name = os.path.basename(data_path)
            self.log.info("file: {0}, shape: {1}, dtype: {2}".format(data_name, sp, self.dtype))
            memmap = np.memmap(data_path, dtype=self.dtype, shape=tuple(sp), mode="r", order="C")
            memmap_list.append(memmap)
        datas = da.concatenate(memmap_list, axis=0)
        datas = da.rechunk(
            datas,
            chunks=(self.chunk_num, datas.shape[1], datas.shape[2])
        )
        self.log.info("Total dat: {0} / {1}".format(datas.shape, datas.dtype))
        return datas
    
    def load_zarr_dask(self, data_paths, grp_cfg):
        """
        Description:
            저장한 zarr를 dask로 불러오기.
        Args:
            data_paths: list, 불러올 파일 리스트
            grp_cfg: namedtuple, Zarr group 설정 
        Returns:
            datas: dask array, Zarr에서 불러온 dask 배열
        """
        datas = [
            da.from_zarr(_zarr, component=grp_cfg.NAME)
            for _zarr in data_paths
        ]
        datas = da.concatenate(datas, axis=0)
        self.log.info("Total zarr: {0} / {1}".format(datas.shape, datas.dtype))
        return datas
    
    def load_npy_dask(self, data_paths):
        """
        Description:
            저장한 npy stack을 dask로 불러오기.
        Args:
            data_paths: list, 불러올 파일 리스트
        Returns:
            datas: dask array, 위성 dask 배열
        """
        datas = [
            da.from_npy_stack(npy_stack_dir, mmap_mode="r")
            for npy_stack_dir in data_paths
        ]
        datas = da.concatenate(datas, axis=0)
        self.log.info("Total npy: {0} / {1}".format(datas.shape, datas.dtype))
        return datas
    
    def fill_zero_frame(self, datas):
        """
        Description:
            학습용 데이터에 결측 처리.
                - dat 파일 생성 시, 전체 결측 파일은 모든 값이 0
            bfill 또는 ffill 방식으로 결측 처리.
                - bfill: 이후 데이터를 limit 개수만큼 복제
                - ffill: 이전 데이터를 limit 개수만큼 복제
            limit를 넘어 결측 처리하지 않는 데이터는 학습에 포함시키지 않음.
        Args:
            datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
        Return:
            datas: dask.array, 결측 처리한 dask array
            nonfill_indices: list, 결측 처리하지 않는 index (학습에도 제외할 데이터)
        """
        # NaN array의 index 찾기
        if self.fill_method:
            sum_0 = da.sum(datas == 0, axis=(1, 2))
            sum_0_mask = da.where(sum_0 > 0, True, False).compute()
            sum_0_indices = np.where(sum_0_mask == True)[0]
            sum_0_len = sum_0_mask.shape[0]
            self.log.info("Fill NaN datas using: {0} / limit frames: {1}".format(self.fill_method, self.fill_limit))
        
        if self.fill_method == "bfill":
            idxes = np.where(~sum_0_mask, np.arange(sum_0_len), sum_0_len-1)
            idxes = np.minimum.accumulate(idxes[::-1], axis=0)[::-1]
        elif self.fill_method == "ffill":
            idxes = np.where(~sum_0_mask, np.arange(sum_0_len), 0)
            idxes = np.maximum.accumulate(idxes, axis=0)
        else:
            # 결측 처리 하지 않은 채로 진행
            return datas, []
        
        # 결측 처리 하지 않을 index 값을 복원
        nonfill_indices = self.make_nonfill_indices(sum_0_indices)
        idxes[nonfill_indices] = nonfill_indices
        self.log.info("Unused datas: {0}".format(len(nonfill_indices)))
        
        return datas[idxes, :, :], nonfill_indices
    
    def make_nonfill_indices(self, sum_0_indices):
        """
        Description:
            결측 처리할 때 limit 범위 밖의 index 계산.
            해당 index 데이터는 결측 처리 진행하지 않음.
        Args:
            sum_0_indices: numpy.array, 결측 데이터의 index
        Return:
            nonfill_indices: list, 결측 처리하지 않는 index (학습에도 제외할 데이터)
        """
        # 연속 결측 데이터 index list 생성
        start_idx_diff = np.ediff1d(np.concatenate((sum_0_indices[:1], sum_0_indices)))
        start_idx_diff_1 = sum_0_indices[np.not_equal(start_idx_diff, 1)]
        end_idx_diff = np.ediff1d(np.concatenate((sum_0_indices, sum_0_indices[-1:])))
        end_idx_diff_1 = sum_0_indices[np.not_equal(end_idx_diff, 1)]

        continuous_indices_bundle = []
        for s_idx, e_idx in zip(start_idx_diff_1, end_idx_diff_1):
            continuous_indices_bundle.append(np.arange(s_idx, e_idx + 1))
        
        # Limit 초과인 경우에 결측처리하지 않을 index 추출
        if self.fill_method == "bfill":
            nonfill_indices_bundle = [
                elm[:-self.fill_limit]
                for elm
                in continuous_indices_bundle
                if elm.shape[0] > self.fill_limit
            ]
        elif self.fill_method == "ffill":
            nonfill_indices_bundle = [
                elm[self.fill_limit:]
                for elm
                in continuous_indices_bundle
                if elm.shape[0] > self.fill_limit
            ]
        else:
            nonfill_indices_bundle = list()
        
        # List flatten
        nonfill_indices = list(chain.from_iterable(nonfill_indices_bundle))
        return nonfill_indices
    
    def get_file_times(self, dat_names):
        """
        Description:
            학습용, 검증용 데이터의 시간대 리스트 산출(KST).
        Args:
            kinds: str, 학습/검증 구분
            dat_names: list, 데이터 파일명(KST)
        Return:
            kst_obj_list: list, dat 파일에서의 KST datetime object의 리스트
        """
        # Extract KST times
        names = [os.path.splitext(dat_name)[0] for dat_name in dat_names]
        name_splits = [name.split("_") for name in names]
        kst_start_times = [splits[-2] for splits in name_splits]
        kst_end_times = [splits[-1] for splits in name_splits]

        # Make UTC times
        total_kst_time_list = list()
        for kst_start_time, kst_end_time in zip(kst_start_times, kst_end_times):
            kst_time_list = self.sat._make_time_list(
                kst_start_time,
                kst_end_time,
                "min",
                self.time_step
            )
            total_kst_time_list.extend(kst_time_list)
        utc_time_list = [self.sat._kst_to_utc(t) for t in total_kst_time_list]
        
        # Adapt Timezone
        kst_obj_list = [
            datetime.strptime(utc_time, self.CVT_TIME_FORMAT).astimezone(self.TZ_KST)
            for utc_time
            in utc_time_list
        ]
        
        return kst_obj_list

    def make_file_exclude_seq_indices(self, dat_names, shape):
        """
        Description:
            파일이 불연속적일 경우 연속적인 Sequence를 만들 수 없는 index 산출.
        Args:
            dat_names: list, 데이터 파일명(KST)
            shape: list, 데이터 shape
        Return:
            file_exclude_seq_indices: list, dat 파일에서의 불연속적 Sequence 생성되는 index
        """
        # Extract KST times
        names = [os.path.splitext(dat_name)[0] for dat_name in dat_names]
        name_splits = [name.split("_") for name in names]
        kst_start_times = [splits[-2] for splits in name_splits]
        kst_end_times = [splits[-1] for splits in name_splits]
        
        # Find Non-continuous times from files
        accum_file_indices = np.cumsum([shp[0] for shp in shape])
        file_exclude_seq_indices = []
        for e_time, s_time, accum_idx in zip(kst_end_times[:-1],
                                             kst_start_times[1:],
                                             accum_file_indices[1:]):
            next_e_time = self.sat._compute_date(e_time, "min", self.time_step)
            if not next_e_time == s_time:
                exclude_seq_indices = list(np.arange(accum_idx - self.total_seq + 1, accum_idx))
                file_exclude_seq_indices.extend(exclude_seq_indices)
        self.log.info("Exclude Non-continuous Sequence indices from .dat files")
        
        return file_exclude_seq_indices

    def compute_mean_std(self, train_datas, train_seq_indices):
        """
        Description:
            전체 학습 데이터에서의 평균, 표준편차 계산.
        Args:
            train_datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
                - Shape: (Data nums, height, width)
            train_seq_indices: list, 학습에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
        Return:
            sat_mean: numpy array, 채널값 평균 (H, W)
            sat_std: numpy array, 채널값 표준편차 (H, W)
        """
        using_train_seq_indices = [
            [j for j in range(i, i+self.in_seq+self.out_seq)]
            for i in train_seq_indices
        ]
        using_train_seq_indices = list(set(chain(*using_train_seq_indices)))
        using_train_datas = train_datas[using_train_seq_indices, ...]
        
        sat_mean = da.nanmean(using_train_datas, axis=0).compute()
        sat_std = da.nanstd(using_train_datas, axis=0).compute()
        self.log.info("Compute Mean & Standard deviation")
        
        return sat_mean, sat_std
    
    def compute_min_max(self, train_datas, train_seq_indices):
        """
        Description:
            전체 학습 데이터에서의 최소, 최대값 계산.
        Args:
            train_datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
                - Shape: (Data nums, height, width)
            train_seq_indices: list, 학습에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
        Return:
            sat_min: numpy array, 채널값 최소 (H, W)
            sat_max: numpy array, 채널값 최대 (H, W)
        """
        using_train_seq_indices = [
            [j for j in range(i, i+self.in_seq+self.out_seq)]
            for i in train_seq_indices
        ]
        using_train_seq_indices = list(set(chain(*using_train_seq_indices)))
        using_train_datas = train_datas[using_train_seq_indices, ...]
        
        sat_min = da.nanmin(using_train_datas, axis=0).compute()
        sat_max = da.nanmax(using_train_datas, axis=0).compute()
        self.log.info("Compute Min & Max")
        
        return sat_min, sat_max
    
    def get_normalizer(self,
                       normalizer_name,
                       calibration_table,
                       sat_mean=None,
                       sat_std=None):
        """
        Description:
            Normalizer 만들기.
        Args:
            normalizer_name: str, normalizer 함수명 (data_parser.py)
            calibration_table: numpy array, 채널의 Calibration table
            sat_mean: numpy array, 채널값 평균 (T, H, W), (default: None)
            sat_std: numpy array, 채널값 표준편차 (T, H, W), (default: None)
        Return:
            normalizer: function, lambda 로 만들어진 normalizer
        """
        self.log.info("Adapt Normalize: {0}".format(normalizer_name))
        norm_cond1 = normalizer_name.startswith("gaussian")
        norm_cond2 = normalizer_name.startswith("inv_gaussian")
        if norm_cond1 or norm_cond2:
            normalizer_args = {"mean": sat_mean, "std": sat_std}
            if not calibration_table is None:
                normalizer_args = self._raw_to_calib_gaussian(normalizer_args,
                                                              calibration_table)
        else:
            normalizer_args = get_arguments(getattr(self.cfg_norm, self.chn.upper()))
            if not calibration_table is None:
                normalizer_args = self._raw_to_calib_minmax(normalizer_args,
                                                            calibration_table)
        normalizer = lambda x: eval(normalizer_name)(x, **normalizer_args)
        return normalizer
    
    def _raw_to_calib_gaussian(self, normalizer_args, calibration_table):
        """
        Description:
            Gaussian Normalizer에서 raw 값이 아닌 Calibration table 값 사용할 때.
            평균 및 표준편차 변환.
        Args:
            normalizer_args: dict, config의 normalizer 옵션
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            normalizer_args: dict, 값이 변경된 config의 normalizer 옵션
        """
        normalizer_args["mean"] = calibration_table[round(normalizer_args["mean"], 0)]
        CALIB_RANGE = abs(np.nanmin(calibration_table) - np.nanmax(calibration_table))
        CALIB_NUM = calibration_table[~np.isnan(calibration_table)].shape[0]
        coeff = CALIB_RANGE / CALIB_NUM
        normalizer_args["std"] = coeff * normalizer_args["std"]
        return normalizer_args
    
    def _raw_to_calib_minmax(self, normalizer_args, calibration_table):
        """
        Description:
            MinMax Normalizer에서 raw 값이 아닌 Calibration table 값 사용할 때.
            VI 및 NR 채널은 raw 값과 Calibration 값의 양의 관계.
            SW, WV, IR 채널은 raw 값과 Calibration 값의 음의 관계.
            SW, WV, IR 채널에서 NaN인 경우는 가장 가까운 nanmin 값 반영.
        Args:
            normalizer_args: dict, config의 normalizer 옵션
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            normalizer_args: dict, 값이 변경된 config의 normalizer 옵션
        """
        if self.chn.startswith("vi") or self.chn.startswith("nr"):
            normalizer_args = {
                k: calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        else:
            k1, k2 = normalizer_args.keys()
            normalizer_args[k1], normalizer_args[k2] = normalizer_args[k2], normalizer_args[k1]
            normalizer_args = {
                k: np.nanmin(calibration_table)
                if np.isnan(calibration_table[v])
                else calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        return normalizer_args
    
    def make_trainable_indices(self,
                               latlon,
                               train_kst_obj_list,
                               valid_kst_obj_list,
                               train_nonfill_indices,
                               valid_nonfill_indices,
                               file_exclude_seq_indices):
        """
        Description:
            학습/검증 데이터의 유효한 Sequence 시작 index 산출.
            Ray 라이브러리 사용 여부는 config에서 제어.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            train_kst_obj_list: list, 학습 dat 파일에서의 KST datetime object의 리스트
            valid_kst_obj_list: list, 검증 dat 파일에서의 KST datetime object의 리스트
            train_nonfill_indices: list, 학습 dat에서 결측 처리하지 않는 index (학습에도 제외할 데이터)
            valid_nonfill_indices: list, 검증 dat에서 결측 처리하지 않는 index (학습에도 제외할 데이터)
            file_exclude_seq_indices: list, dat 파일에서의 불연속적 Sequence 생성되는 index
        Return:
            train_seq_indices: list, 학습에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
            valid_seq_indices: list, 검증에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
        """
        self.calib.ray_start()
        
        # Make Sequence indices
        west_pixel, east_pixel = self.calib.setting_sza(latlon)
        train_seq_indices = self.calib.make_seq_indices(latlon,
                                                        west_pixel,
                                                        east_pixel,
                                                        train_kst_obj_list,
                                                        self.total_seq)
        valid_seq_indices = self.calib.make_seq_indices(latlon,
                                                        west_pixel,
                                                        east_pixel,
                                                        valid_kst_obj_list,
                                                        self.total_seq)
        
        # Make Excluding Sequence indices
        train_unused_indices = train_nonfill_indices + file_exclude_seq_indices
        valid_unused_indices = valid_nonfill_indices + file_exclude_seq_indices
        train_seq_indices = self.calib.remove_unused_indices(train_seq_indices,
                                                             train_unused_indices)
        valid_seq_indices = self.calib.remove_unused_indices(valid_seq_indices,
                                                             valid_unused_indices)
        self.log.info("Sequence init indices nums (Train): {0}".format(len(train_seq_indices)))
        self.log.info("Sequence init indices nums (Valid): {0}".format(len(valid_seq_indices)))
        
        self.calib.ray_stop()
        
        return train_seq_indices, valid_seq_indices

    def infer_prep(self, infer_datas, utc_time_list, calibration_table):
        """
        Description:
            추론 전처리.
            추론에 사용할 입력 시계열 데이터 생성.
                - 파일이 없어 결측 처리 안된 부분을 전후 데이터로 결측 처리
                - dataloader 생성
        Args:
            infer_datas: np.array, 모델 입력 데이터(추론)
            utc_time_list: list, 입력 데이터의 UTC 시간
            calibration_table: numpy array, 채널의 Calibration table
        Returns:
            infer_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - Shape: (batch_size, input_seq_len, height, width, 1)
        """
        infer_datas, _ = self.fill_zero_frame(infer_datas)
        infer_seq_indices = [i for i in range(len(utc_time_list))]
        
        # Normalizer
        normalizer_name = self.cfg_norm.FUNCTION
        sat_mean, sat_std = None, None
        if normalizer_name is None:
            normalizer = None
        elif normalizer_name.startswith("gaussian"):
            sat_mean, sat_std = self.load_mean_std(self.load_model_dir)
            normalizer = self.get_normalizer(normalizer_name,
                                             calibration_table,
                                             sat_mean,
                                             sat_std)
        else:
            normalizer = self.get_normalizer(normalizer_name,
                                             calibration_table)
        
        # Make dataset
        infer_dataset = GK2ADatasetV3(
            memory_map=infer_datas,
            in_seq=self.in_seq,
            input_shape=self.input_shape,
            normalizer=normalizer,
            calibration_table=calibration_table,
            seq_indices=infer_seq_indices,
            mean=sat_mean,
            std=sat_std,
            pred_mode=self.pred_mode
        )
        self.log.info("Able to make Seq nums: {0}".format(infer_dataset.batch_seq_nums))
        self.log.info("Numbers of Origin -> Crop size : {0}".format(infer_dataset.split_nums))
        self.log.info("Using Infer Data nums: {0}".format(len(infer_dataset)))
        
        # Make dataloader
        infer_dl = self.make_openstl_dl(infer_dataset, self.infer_batch_size)
        
        return infer_dl

    def get_infer_datas(self,
                        kst_start_time,
                        kst_end_time):
        """
        Description:
            추론에 사용할 데이터 불러오기.
        Args:
            kst_start_time: str, 추론할 기준 시작시점(연월일시분 12자리)
            kst_end_time: str, 추론할 기준 최종시점(연월일시분 12자리)
        Returns:
            datas: np.array, 모델 입력 데이터(추론)
                - Shape: (Data nums, height, width)
        """
        self.log.info("Load Predict datas")
        datas = self.sat(kst_start_time, kst_end_time, self.time_step)
        self.log.info("Predict data shape: {0}".format(datas.shape))
        return datas
    
    def make_openstl_dl(self, dataset, batch_size):
        """
        Description:
            Torch Dataloader 생성.
        Args:
            dataset: torch.Dataset, 배치 사이즈 처리만 하면 되는 데이터
            batch_size: int, 배치 사이즈
        Returns:
            dl: torch.Dataloader, 데이터 로더
        """
        from torch.utils.data import (
            DataLoader, RandomSampler, BatchSampler, SequentialSampler
        )
        if self.pred_mode:
            sampler = SequentialSampler(dataset)
        else:
            sampler = RandomSampler(dataset)
        batch_sampler = BatchSampler(
            sampler,
            batch_size=batch_size,
            drop_last=False
        )
        dl = DataLoader(
            dataset,
            batch_sampler=batch_sampler,
            num_workers=self.dl_num_workers,
            pin_memory=self.dl_pin_mem
        )
        return dl
    
    def save_mean_std(self, save_dir, sat_mean, sat_std):
        """
        Description:
            전체 학습 데이터에서의 평균, 표준편차 npy 저장.
        Args:
            save_dir: str, npy 저장할 디렉토리 경로
            sat_mean: numpy array, 채널값 평균 (H, W)
            sat_std: numpy array, 채널값 표준편차 (H, W)
        Return:
            None
        """
        mean_path = os.path.join(save_dir, self.gaussian_mean_file)
        std_path = os.path.join(save_dir, self.gaussian_std_file)
        np.save(mean_path, sat_mean)
        np.save(std_path, sat_std)
    
    def load_mean_std(self, save_dir):
        """
        Description:
            전체 학습 데이터에서의 평균, 표준편차 npy 불러오기.
        Args:
            save_dir: str, npy 불러올 디렉토리 경로
        Return:
            sat_mean: numpy array, 채널값 평균 (H, W)
            sat_std: numpy array, 채널값 표준편차 (H, W)
        """
        mean_path = os.path.join(save_dir, self.gaussian_mean_file)
        std_path = os.path.join(save_dir, self.gaussian_std_file)
        sat_mean = np.load(mean_path)
        sat_std = np.load(std_path)
        return sat_mean, sat_std
    
    def save_min_max(self, save_dir, sat_min, sat_max):
        """
        Description:
            전체 학습 데이터에서의 평균, 표준편차 npy 저장.
        Args:
            save_dir: str, npy 저장할 디렉토리 경로
            sat_min: numpy array, 채널값 최소값 (H, W)
            sat_max: numpy array, 채널값 최댓값 (H, W)
        Return:
            None
        """
        min_path = os.path.join(save_dir, self.minmax_min_file)
        max_path = os.path.join(save_dir, self.minmax_max_file)
        np.save(min_path, sat_min)
        np.save(max_path, sat_max)
    
    def load_min_max(self, save_dir):
        """
        Description:
            전체 학습 데이터에서의 최소값, 최대값 npy 불러오기.
        Args:
            save_dir: str, npy 불러올 디렉토리 경로
        Return:
            sat_min: numpy array, 채널값 최소값 (H, W)
            sat_max: numpy array, 채널값 최댓값 (H, W)
        """
        min_path = os.path.join(save_dir, self.minmax_min_file)
        max_path = os.path.join(save_dir, self.minmax_max_file)
        sat_min = np.load(min_path)
        sat_max = np.load(max_path)
        return sat_min, sat_max


class SatPrepV2(SatPrep):
    """Preprocess using Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log, pred_mode=False):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 전처리 init 설정.
            위성 영상 변화량으로 진행.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
            pred_mode: bool, 추론 모드 판단
        """
        super(SatPrepV2, self).__init__(cfg, log, err_log, pred_mode)
    
    def __call__(self, kst_start_time=None, kst_end_time=None):
        """
        Description:
            학습, 추론 전처리.
            GK2A 태양천정각 사용 분기 적용.
                - 태양천정각: 주간, 야간, 여명/황혼 분리.
        Args:
            학습 전처리
                None
            추론 전처리
                - kst_start_time: str, 추론할 기준 시작시점(연월일시분 12자리)
                - kst_end_time: str, 추론할 기준 최종시점(연월일시분 12자리)
        Returns:
            학습 전처리
                - train_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - valid_dl: torch.Dataloader 객체, Dataloader 역할(검증)
                - normalizer_args: dict, normalizer의 인자값
            추론 전처리
                - infer_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        """
        # Calibration
        # calibration_table = self.calib.make_calibration_table()
        latlon = self.calib.load_latlon()
        
        # Branch Training mode or Prediction mode
        if not self.pred_mode:
            train_dl, valid_dl, normalizer_args = self.train_prep(latlon)
            return train_dl, valid_dl, normalizer_args
        else:
            pass
            # infer_datas = self.get_infer_datas(kst_start_time,
            #                                    kst_end_time)
            # kst_time_list = self.sat._make_time_list(kst_start_time,
            #                                          kst_end_time,
            #                                          "min",
            #                                          self.time_step)
            # utc_time_list = [self.sat._kst_to_utc(t) for t in kst_time_list]
            # infer_dl = self.infer_prep(infer_datas,
            #                            utc_time_list,
            #                            calibration_table)
            # return infer_dl, utc_time_list
    
    def train_prep(self, latlon):
        """
        Description:
            학습 전처리.
            Calibration은 불러오는 파일에 이미 적용되어 있어야 효율적.
            모델에 사용할 dataloader 생성.
                - dat 파일 불러오기
                - 파일이 없어 결측 처리 안된 부분을 전후 데이터로 결측 처리
                - 연속적인 시간대 Sequence의 index를 추출
                - 변화량(delta)로 데이터 및 index 변환
                - dataloader 생성
            평균, 표준편차는 저장을 위해 반환.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            train_dl: torch.Dataloader 객체, Dataloader 역할(학습)
                - Input Shape: (batch_size, input_seq_len, 1, height, width)
                - Output Shape: (batch_size, output_seq_len, 1, height, width)
            valid_dl: torch.Dataloader 객체, Dataloader 역할(검증)
                - Input Shape: (batch_size, input_seq_len, 1, height, width)
                - Output Shape: (batch_size, output_seq_len, 1, height, width)
            normalizer_args: dict, normalizer에 반영할 인자
                - gaussian: {"mean": sat_mean, "std": sat_std}
                - minmax: {"min_value": sat_min, "max_value": sat_max}
        """
        train_datas = self.get_train_datas(
            "Train", self.dat_names, self.shape, self.saved_method
        )
        train_datas, train_nonfill_indices = self.fill_zero_frame(train_datas)
        valid_datas = self.get_train_datas(
            "Valid", self.valid_dat_names, self.valid_shape, self.saved_method
        )
        valid_datas, valid_nonfill_indices = self.fill_zero_frame(valid_datas)
        
        # SZA 사용여부는 make_trainable_indices 에서 처리
        train_kst_obj_list = self.get_file_times(self.dat_names)
        valid_kst_obj_list = self.get_file_times(self.valid_dat_names)
        file_exclude_seq_indices = self.make_file_exclude_seq_indices(
            self.dat_names, self.shape
        )
        train_seq_indices, valid_seq_indices = self.make_trainable_indices(
            latlon,
            train_kst_obj_list, valid_kst_obj_list,
            train_nonfill_indices, valid_nonfill_indices,
            file_exclude_seq_indices
        )
        
        # Make delta data => mean, std 구하는 데만 이용
        delta_datas, delta_seq_indices, _ = self.make_delta(
            train_datas, train_seq_indices, train_kst_obj_list
        )
        
        # Normalizer
        normalizer_name = self.cfg_norm.FUNCTION
        normalizer, normalizer_args = self.get_normalizer(
            normalizer_name, delta_datas, delta_seq_indices
        )
        del delta_datas, delta_seq_indices
        
        # Make dataset
        train_ds = GK2ADatasetV4(
            datas=train_datas,
            seq_indices=train_seq_indices,
            in_seq=self.in_seq,
            out_seq=self.out_seq,
            normalizer=normalizer,
            normalizer_args=normalizer_args,
            pred_mode=self.pred_mode
        )
        valid_ds = GK2ADatasetV4(
            datas=valid_datas,
            seq_indices=valid_seq_indices,
            in_seq=self.in_seq,
            out_seq=self.out_seq,
            normalizer=normalizer,
            normalizer_args=normalizer_args,
            pred_mode=self.pred_mode
        )
        self.log.info("Using Train Dataset nums: {0}".format(len(train_ds)))
        self.log.info("Using Valid Dataset nums: {0}".format(len(valid_ds)))
        
        # Make dataloader
        train_dl = self.make_openstl_dl(
            train_ds, self.in_seq, self.batch_size
        )
        valid_dl = self.make_openstl_dl(
            valid_ds, self.in_seq, self.infer_batch_size
        )
        
        return train_dl, valid_dl, normalizer_args
    
    def calibrate_sat(self, datas, calibration_table):
        """
        Description:
            Calibration 테이블 값으로 변환.
        Args:
            datas: dask.array, 위성 원본 데이터
                - Shape: (Data nums, height, width)
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            calib_datas: dask.array, Calibration 적용된 위성 데이터
        """
        if isinstance(calibration_table, np.ndarray):
            self.log.info("Using Calibration table")
            calib_datas = calibration_table[datas]   # 자동 compute
            calib_datas = da.from_array(
                calib_datas,
                chunks=(self.chunk_num, datas.shape[1], datas.shape[2])
            )
            return calib_datas
        else:
            self.log.info("Passing Calibration table")
            return datas
    
    def make_delta(self, datas, seq_indices, kst_list):
        """
        Description:
            변화량 값 계산 및 관련 리스트 길이 조정.
        Args:
            datas: dask.array, 위성 데이터
                - Shape: (Data nums, height, width)
            seq_indices: list, 사용할 sequence 생성 가능한 시간대 index
            kst_list: list: KST datetime object의 리스트
        Return:
            delta_datas: dask.array, 위성 데이터 변화량
            delta_seq_indices: list, 사용할 sequence 생성 가능한 시간대 index
            delta_kst_list: list, KST datetime object의 리스트
        """
        delta_datas = datas[1:] - datas[:-1]
        delta_seq_indices = [i-1 for i in seq_indices if i > 0]
        delta_kst_list = list(np.array(kst_list[1:])[delta_seq_indices])
        return delta_datas, delta_seq_indices, delta_kst_list
    
    def get_normalizer(self,
                       normalizer_name,
                       train_datas,
                       train_seq_indices):
        """
        Description:
            Normalizer 만들기.
        Args:
            normalizer_name: str, normalizer 함수명 (data_parser.py)
            train_datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
                - Shape: (Data nums, height, width)
            train_seq_indices: list, 학습에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
        Return:
            normalizer: function, lambda 로 만들어진 normalizer
            normalizer_args: dict, normalizer에 적용할 값
        """
        self.log.info("Adapt Normalize: {0}".format(normalizer_name))
        re_gaussian = re.compile("gaussian")
        re_minmax = re.compile("min_max")
        if normalizer_name is None:
            normalizer = None
        elif re_gaussian.search(normalizer_name) is not None:
            sat_mean, sat_std = self.compute_mean_std(
                train_datas, train_seq_indices
            )
            normalizer_args = {"mean": sat_mean, "std": sat_std}
        elif re_minmax.search(normalizer_name) is not None:
            sat_min, sat_max = self.compute_min_max(
                train_datas, train_seq_indices
            )
            normalizer_args = {"min_value": sat_min, "max_value": sat_max}
        else:
            import src.data_parser as dp
            self.err_log.error("Write correct normalizer_name: {0}.".format(normalizer_name))
            self.err_log.error("Just use {0}".format([elm for elm in dir(dp) if elm.endswith("normalizer")]))
        normalizer = lambda x: eval(normalizer_name)(x, **normalizer_args)
        return normalizer, normalizer_args
    
    def make_openstl_dl(self, dataset, in_seq, batch_size):
        """
        Description:
            Torch Dataloader 생성.
        Args:
            dataset: torch.Dataset, 배치 사이즈 처리만 하면 되는 데이터
            in_seq: int, 입력 시퀀스 길이
            batch_size: int, 배치 사이즈
        Returns:
            dl: torch.Dataloader, 데이터 로더
        """
        if self.pred_mode:
            sampler = SequentialSampler(dataset)
        else:
            sampler = RandomSampler(dataset)
        batch_sampler = BatchSampler(
            sampler,
            batch_size=batch_size,
            drop_last=False
        )
        sat_collate = SatCollator(in_seq, self.dask_workers)
        loader_args = dict(
            num_workers=self.dl_num_workers,
            batch_sampler=batch_sampler,
            collate_fn=sat_collate,
            pin_memory=self.dl_pin_mem,
            worker_init_fn=partial(worker_init, worker_seeding="all"),
            persistent_workers=False
        )
        dl = DataLoader(dataset, **loader_args)
        # dl = DataLoader(
        #     dataset,
        #     batch_sampler=batch_sampler,
        #     num_workers=self.dl_num_workers,
        #     pin_memory=self.dl_pin_mem,
        #     # multiprocessing_context="fork",   # test
        #     collate_fn=sat_collate
        # )
        return dl
