import datetime
import os
import numpy as np
import netCDF4 as nc

from src.sat_calibration import SatCalibration

import warnings
warnings.filterwarnings(action="ignore")


class Data(object):
    """공통 부분."""
    def __init__(self):
        """초기값."""
        super(Data, self).__init__()
        self.CVT_TIME_FORMAT = "%Y%m%d%H%M"
        self.UTC2KST = 9
        self.KST2UTC = self.UTC2KST * (-1)
    
    def _kst_to_utc(self, date):
        """
        Description:
            KST 기준 시간을 UTC로 변경.
        Args:
            date: str, KST 연월일시분(12자리)
        Returns:
            date: str, UTC 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += datetime.timedelta(hours=self.KST2UTC)
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _utc_to_kst(self, date):
        """
        Description:
            UTC 기준 시간을 KST로 변경.
        Args:
            date: str, UTC 연월일시분(12자리)
        Returns:
            date: str, KST 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += datetime.timedelta(hours=self.UTC2KST)
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _compute_date(self, date, standard, time_step):
        """
        Description:
            분단위 기준으로 이후 시간 계산.
        Args:
            date: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격(양수면 이후, 음수면 이전)
        Returns:
            date: str, 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        if standard == "day":
            date += datetime.timedelta(days=time_step)
        elif standard == "hour":
            date += datetime.timedelta(hours=time_step)
        elif standard == "min":
            date += datetime.timedelta(minutes=time_step)
        elif standard == "sec":
            date += datetime.timedelta(seconds=time_step)
        else:
            print("Wrong time step standard: {0}".format(standard))
            print("Just using: day, hour, min, sec")
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _make_time_list(self,
                        start_time,
                        end_time,
                        standard,
                        time_step):
        """
        Description:
            시간대 리스트 생성(시작시점~종료시점).
        Args:
            start_time: str, 연월일시분(12자리)
            end_time: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격
        Returns:
            time_list: list, 문자열 시간대 리스트
        """
        time_list = []
        while start_time <= end_time:
            time_list.append(start_time)
            start_time = self._compute_date(start_time,
                                            standard,
                                            time_step)
        return time_list
    
    def _make_time_list_v2(self,
                           start_time,
                           seq_len,
                           standard,
                           time_step,
                           direction):
        """
        Description:
            시간대 리스트 생성(기준 시점 포함한 시간 길이).
        Args:
            start_time: str, 연월일시분(12자리)
            seq_len: int, 시간대 개수
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격
            direction: str, before, after만 가능, 기준 시점에서 과거/미래 방향
        Returns:
            time_list: list, 문자열 시간대 리스트
        """
        if direction == "before":
            coeff = -1
        elif direction == "after":
            coeff = 1
        time_list = []
        START_NUM = 0
        while START_NUM < seq_len:
            time_list.append(start_time)
            start_time = self._compute_date(start_time,
                                            standard,
                                            coeff*time_step)
            START_NUM += 1
        return sorted(time_list)


class GK2ADataExtractor(Data):
    def __init__(self, cfg, log, err_log, pred_mode=False):
        """
        Description:
            위성(GK2A) 데이터 불러오는 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
            pred_mode: bool, 추론 모드 판단
        """
        super(GK2ADataExtractor, self).__init__()
        cfg_r = cfg.RAWDATA
        cfg_d = cfg.PARAMS.DATA
        cfg_tr = cfg.TRAIN

        self.pred_mode = pred_mode
        self.chn = cfg_d.CHANNEL
        self.ny, self.nx = cfg_r.LATALON_RESOLUTION
        self.input_seq = cfg_d.INPUT_SEQ
        
        # 데이터
        # self.sat_dir = os.path.join(cfg_r.INPUT_DIR, self.chn)   # 78 server
        self.sat_dir = os.path.join(cfg_r.INPUT_DIR)   # 76 server
        self.sat_file = cfg_r.INPUT_FILE
        self.ARRAY_TYPE = getattr(np, cfg_r.DATA_TYPE)
        self.SAVE_ARRAY_TYPE = getattr(np, cfg_r.SAVE_DATA_TYPE)
        self.NONE_CHECK_LOOP = cfg_r.NONE_CHECK_LOOP
        # 학습/검증 데이터 저장
        self.data_saved_dir = cfg_tr.DATA_DIR
        self.data_saved_file = cfg_tr.DATA_NAME_FORMAT
        
        self.calib = SatCalibration(cfg, log, err_log)
        self.USE_CALIB = cfg_r.USE_CALIB
        self.crop_area = cfg_d.CROP_AREA.USE

        self.log, self.err_log = log, err_log

    def __call__(self,
                 kst_start_time,
                 kst_end_time,
                 time_step=10):
        """
        Description:
            레이더 데이터를 numpy 배열로 불러오기.
            Calibration 적용도 결정.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            time_step: int, standard에서 증감할 시간 간격(분), default=10
        Returns:
            학습
                - sat_array: numpy array, 전체 위성 배열
                - kst_time: str, 마지막 KST 시간대
            추론
                - sat_array: numpy array, 전체 위성 배열
        """
        if not self.pred_mode:
            kst_time_list = self._make_time_list(kst_start_time,
                                                 kst_end_time,
                                                 "min",
                                                 time_step)
            self.log.info(">>> Using Channel / Init date(KST) / End date(KST): {0} / {1} / {2}".format(self.chn,
                                                                                                       kst_time_list[0],
                                                                                                       kst_time_list[-1]))
            self.log.info(">>> Using Channel / Init date(UTC) / End date(UTC): {0} / {1} / {2}".format(self.chn,
                                                                                                       self._kst_to_utc(kst_time_list[0]),
                                                                                                       self._kst_to_utc(kst_time_list[-1])))
            self.log.info(">>> Time counts: {0}".format(len(kst_time_list)))
            sat_array = self._make_sat_ary(kst_time_list, False)
            if self.USE_CALIB:
                sat_array = self._calibrate_sat(sat_array)
            return sat_array, kst_time_list[-1]
        else:
            bf_kst_time_list = self._make_time_list_v2(kst_start_time,
                                                       self.input_seq,
                                                       "min",
                                                       time_step,
                                                       "before")
            af_kst_time_list = self._make_time_list(kst_start_time,
                                                    kst_end_time,
                                                    "min",
                                                    time_step)
            kst_time_list = bf_kst_time_list + af_kst_time_list[1:]   # 중복 시간대 제외
            self.log.info(">>> Using Channel / Init date(KST) / End date(KST): {0} / {1} / {2}".format(self.chn,
                                                                                                       kst_time_list[0],
                                                                                                       kst_time_list[-1]))
            self.log.info(">>> Using Channel / Init date(UTC) / End date(UTC): {0} / {1} / {2}".format(self.chn,
                                                                                                       self._kst_to_utc(kst_time_list[0]),
                                                                                                       self._kst_to_utc(kst_time_list[-1])))
            sat_array = self._make_sat_ary(kst_time_list, True)
            if self.USE_CALIB:
                sat_array = self._calibrate_sat(sat_array)
            return sat_array
    
    def _make_sat_ary(self, kst_time_list, pred_mode):
        """
        Description:
            위성 데이터로 numpy 배열 생성.
            일부 영역만 사용할 경우, 해당 영역만 슬라이싱.
                - effective_area_pixel 계산하여 적용
        Args:
            kst_time_list: list, KST 문자열 시간대 리스트
            pred_mode: bool, 추론 모드 판단
        Returns:
            sat_array: numpy array, 전체 위성 배열
        """
        latlon = self.calib.load_latlon()
        if pred_mode:
            self.log.info(">>> Crop: Outline")
            effective_area_pixel = self.calib.make_effective_area_pixel_outline(latlon)
        elif self.crop_area:
            self.log.info(">>> Crop: Center")
            effective_area_pixel = self.calib.make_effective_area_pixel_center(latlon)
        else:
            self.log.info(">>> Crop: None")
            effective_area_pixel = (None, None, None, None)
        
        y_min, y_max, x_min, x_max = effective_area_pixel
        self.log.info(">>> Effective Area Pixel - lat: ({0}, {1}), lon: ({2}, {3})".format(y_min, y_max, x_min, x_max))
        if y_min is None:
            sat_array = np.zeros(
                 (len(kst_time_list), self.ny, self.nx),
                 dtype=self.ARRAY_TYPE
            )
        else:
            self.log.info(">>> Crop Data")
            sat_array = np.zeros(
                (len(kst_time_list), y_max-y_min, x_max-x_min),
                dtype=self.ARRAY_TYPE
            )
        
        for idx, kst_time in enumerate(kst_time_list):
            sat_data = self._load_sat_data(kst_time)
            if y_min is not None:
                sat_data = sat_data[y_min:y_max, x_min:x_max]
            sat_array[idx] = sat_data
        self.log.info(">>> Original Data Shape: {0}".format(sat_array.shape))
        return sat_array
    
    def _load_sat_data(self, kst_time):
        """
        Description:
            위성 데이터(netCDF 파일) 불러오기.
            해당 시간대에 데이터가 없으면 2분 전 데이터를 사용.
            MAX_LOOP 만큼 진행해도 없으면 NaN 배열로 사용.
        Args:
            kst_time: str, KST 연월일시분(12자리)
        Returns:
            rdr_dBz: numpy array, 레이더 dBZ 배열
        """
        utc_time = self._kst_to_utc(kst_time)
        sat_file = self.sat_file.format(self.chn, utc_time)
        sat_path = os.path.join(self.sat_dir, sat_file)   # 78 server
        sat_path = os.path.join(
            self.sat_dir,
            utc_time[:6],
            utc_time[6:8],
            utc_time[8:10],
            sat_file
        )
        
        # 위성 데이터 있는지 확인 후 없을 시 2분전 데이터를 가져옴(MAX_LOOP까지 반복)
        NOW_LOOP = 1
        while not os.path.isfile(sat_path):
            self.log.info(">>> File doesn't exists: {0}(UTC) / {1}(KST)".format(utc_time, kst_time))
            self.log.debug(">>> File doesn't exists: {0}".format(sat_path))
            utc_time = self._compute_date(utc_time, "min", -2)
            sat_file = self.sat_file.format(self.chn, utc_time)
            sat_path = os.path.join(self.sat_dir, sat_file)   # 78 server
            sat_path = os.path.join(
                self.sat_dir,
                utc_time[:6],
                utc_time[6:8],
                utc_time[8:10],
                sat_file
            )   # 76 server
            NOW_LOOP += 1
            if NOW_LOOP > self.NONE_CHECK_LOOP:
                self.log.info(">>> File doesn't exists: {0}(UTC) / {1}(KST)".format(utc_time, kst_time))
                break
        
        # 파일이 존재해도 netCDF가 열리지 않으면 NaN 처리
        CAN_OPEN = True
        try:
            with nc.Dataset(sat_path, "r", format="netcdf4") as sat_nc:
                ipixel = sat_nc.variables["image_pixel_values"]
                ipixel_ary = np.array(ipixel)
                # set error pixel's value to 0
                # 16384~32767(조건에 따라 사용 가능), 32768(관측 영역 외부) 및 49152~65535(오류 존재)
                ipixel_ary[ipixel_ary > 49151] = 0   # set error pixel's value to 0
                # image_pixel_values Bit Size per pixel masking
                channel = ipixel.getncattr("channel_name")
                if ((channel == "VI004") or (channel == "VI005") or (channel == "NR016")):
                    mask = 0b0000011111111111 #11bit mask
                elif ((channel == "VI006") or (channel == "NR013") or (channel == "WV063")):
                    mask = 0b0000111111111111 #12bit mask
                elif (channel == "SW038"):
                    mask = 0b0011111111111111 #14bit mask
                else:
                    mask = 0b0001111111111111 #13bit mask
                sat_data = np.bitwise_and(ipixel_ary, mask)
                
        except Exception as e:
            self.err_log.error(">>> File cannot open {0}(UTC) / {1}(KST): {2}".format(utc_time, kst_time, e))
            CAN_OPEN = False
        
        if CAN_OPEN:
            sat_data = sat_data.astype(self.ARRAY_TYPE)
        else:
            self.log.info(">>> Cannot Used File: {0}(UTC) / {1}(KST) --> Make NaN array".format(utc_time, kst_time))
            sat_data = np.ones((self.ny, self.nx), dtype=self.ARRAY_TYPE) * np.nan

        return sat_data
    
    def _calibrate_sat(self, sat_array):
        """
        Description:
            Calibration 테이블 값으로 변환.
        Args:
            sat_array: numpy array, 전체 위성 배열
                - Shape: (Data nums, height, width)
        Return:
            calib_datas: numpy array, Calibration 적용된 위성 데이터
        """
        calibration_table = self.calib.make_calibration_table()
        if isinstance(calibration_table, np.ndarray):
            self.log.info("Using Calibration table")
            calib_datas = calibration_table[sat_array]
            return calib_datas
        else:
            self.log.info("Passing Calibration table")
            return sat_array
    
    def save_sat_ary(self,
                     sat_array,
                     kst_start_time,
                     kst_end_time):
        """
        Description:
            위성 데이터 배열 저장.
            memmap으로 저장 -> 추후 불러올 때 memmap 사용
            save로 저장 -> 추후 불러올 때 load 사용
        Args:
            sat_array: numpy array, 전체 위성 데이터 배열
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        saved_file = self.data_saved_file.format(self.chn, kst_start_time, kst_end_time)
        saved_path = os.path.join(self.data_saved_dir, saved_file)
        memmap = np.memmap(saved_path,
                           dtype=self.SAVE_ARRAY_TYPE,
                           mode="w+",
                           shape=sat_array.shape)
        memmap[:] = sat_array[:]
        self.log.info(">>> Save completely")


if __name__ == "__main__":
    from setproctitle import setproctitle
    setproctitle("extract_gk2a")
    
    from log_module import LogConfig, get_log_view
    from ..utils.code_utils import convert

    # 로그
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    file_prefix = "extract_gk2a"
    lc = LogConfig(upper_dir, file_prefix)
    # log = get_log_view(lc, log_level="DEBUG")   # default: INFO
    log = get_log_view(lc)   # default: INFO
    err_log = get_log_view(lc, log_level="WARNING", error_log=True)
    
    cfg_file = "/fogdata/sat_pred/sat_convlstm/tf_ver/configs/sat_pred_cfg.yaml"
    cfg = convert(cfg_file)
    chn = cfg.PARAMS.DATA.CHANNEL

    kst_start_time_list = cfg.RAWDATA.KST_START_TIME
    kst_end_time_list = cfg.RAWDATA.KST_END_TIME
    time_step = cfg.RAWDATA.TIME_STEP
    
    # Make GK2A.dat
    sat = GK2ADataExtractor(cfg, log, err_log)
    for kst_start_time, kst_end_time in zip(kst_start_time_list, kst_end_time_list):
        sat_ary, file_kst_end_time = sat(kst_start_time, kst_end_time, time_step)
        sat.save_sat_ary(sat_ary, kst_start_time, file_kst_end_time)
