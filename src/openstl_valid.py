import os
import time
import numpy as np
import pandas as pd
from collections import defaultdict

from src.openstl_make_img import SatImg
from utils.code_utils import return_dir
from utils.log_module import print_elapsed_time


class SatValid(SatImg):
    """Make Validation results using Satellite prediction & real."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            예측 및 실제 위성(GK2A) 영상으로 검증 결과 산출.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatValid, self).__init__(cfg, log, err_log)   # 상속
        cfg_v = cfg.VALID
        self.round_num = cfg_v.ROUND_NUM
        self.valid_df_cols = cfg_v.VALID_COLS
        self.saved_file = cfg_v.OUTPUT_FILE
        self.valid_chn_dir = return_dir(False,
                                        self.output_dir,
                                        "{0}_valid".format(self.chn))
        self.making_real = True   # SatImg에서 True 여야 실제 결과 산출함
        self.log, self.err_log = log, err_log

    def __call__(self, kst_start_time, kst_end_time):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지로 검증결과 산출.
            kst_start_time ~ kst_end_time 사이 시간을 ticket으로 사용.
            파일명은 UTC를 사용.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        self.log.info(">>> Start to Validate Satellite datas")
        start_time = time.time()

        TICKET_LEN = self._ticket_time_len(kst_start_time, kst_end_time)
        
        # 실제 위성 이미지
        self.log.info(">>> Load Real Satellite datas")
        real_sat_array, utc_time_list = self.load_real_sat(kst_start_time, kst_end_time)
        self.log.info(">>> Load Real Satellite datas completely")
        
        # 추론 위성 이미지 결과
        self.log.info(">>> Load Predicted Satellite datas")
        ticket_utc_time_list = utc_time_list[:TICKET_LEN]
        pred_sat_array_list = []
        for idx in range(TICKET_LEN):
            ticket_utc_time = ticket_utc_time_list[idx]
            pred_utc_time_list = utc_time_list[idx+1:idx+1+self.result_seq]
            self.log.info(">>> Ticket UTC time: {0}".format(ticket_utc_time))
            pred_sat_array = self.load_pred_sat(ticket_utc_time,
                                                pred_utc_time_list)
            pred_sat_array_list.append(pred_sat_array)
        self.log.info(">>> Load Predicted Satellite datas completely")
        
        # 검증결과 산출
        self.log.info(">>> Compute & Save Validation values of Satellite datas")
        valid_df_list = self.valid_datas(utc_time_list,
                                         ticket_utc_time_list,
                                         real_sat_array,
                                         pred_sat_array_list)
        self.log.info(">>> Compute & Save Validation values of Satellite datas completely")
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Validate Satellite datas")
        
        return valid_df_list, ticket_utc_time_list

    def valid_datas(self,
                    time_list,
                    ticket_time_list,
                    real_sat_array,
                    pred_sat_array_list):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지로 검증결과 계산.
            전체 및 pred_time 각각 검증결과 계산.
            전체 계산결과는 ticket_time과 pred_time 값을 동일하게 설정.
        Args:
            time_list: list, 전체 시간대 리스트 (UTC)
            ticket_time_list: list, ticket에 해당하는 시간대 리스트 (UTC)
            real_sat_array: numpy array, ticket_time로 추론한 시간대의 실제 위성 이미지들 전체
            pred_sat_array_list: list, ticket_time로 추론한 시간대의 예측 위성 이미지들 전체
        Returns:
            valid_df_list: list, 전체 검증결과 list
        """
        valid_df_list = []
        for idx, (ticket, pred_array) in enumerate(zip(ticket_time_list, pred_sat_array_list)):
            pred_utc_time_list = time_list[idx:idx+1+self.result_seq]
            valid_real_sat_array = real_sat_array[idx:idx+self.result_seq]
            valid_df = self.valid_one_time(ticket,
                                           pred_utc_time_list,
                                           valid_real_sat_array,
                                           pred_array)
            valid_df_list.append(valid_df)
        return valid_df_list

    def valid_one_time(self,
                       ticket_time,
                       pred_time_list,
                       real_array,
                       pred_array):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지로 검증결과 계산.
            전체 및 pred_time 각각 검증결과 계산.
            전체 계산결과는 ticket_time과 pred_time 값을 동일하게 설정.
        Args:
            ticket_time: str, 연월일시분(12자리)
            pred_time_list: list, 연월일시분(12자리)이 원소인 추론 시간대 리스트
            real_array: numpy array, ticket_time로 추론한 시간대의 실제 위성 이미지들
            pred_array: numpy array, ticket_time로 추론한 시간대의 예측 위성 이미지들
        Returns:
            valid_df: dataframe, 해당 ticket 시간대 검증 결과
        """
        valid_dict = defaultdict(list)
        use_cols = self.valid_df_cols
        
        # 검증값 계산
        ticket_mse_all = self.compute_mse(real_array,
                                            pred_array)
        ticket_mae_all = self.compute_mae(real_array,
                                            pred_array)
        
        # 추론 시간대 별 계산
        use_axes = tuple([i for i in range(len(pred_array.shape))])[1:]   
        ticket_mse_times = self.compute_mse(real_array,
                                            pred_array,
                                            use_axes)
        ticket_mae_times = self.compute_mae(real_array,
                                            pred_array,
                                            use_axes)
        
        # DataFrame으로 생성
        ticket_mse_list = [ticket_mse_all] + list(ticket_mse_times)
        ticket_mae_list = [ticket_mae_all] + list(ticket_mae_times)
        valid_dict[use_cols[2]] = pred_time_list
        valid_dict[use_cols[3]] = ticket_mse_list
        valid_dict[use_cols[4]] = ticket_mae_list
        
        valid_df = pd.DataFrame(valid_dict)
        valid_df[use_cols[1]] = ticket_time
        valid_df[use_cols[0]] = self.chn
        
        return valid_df[self.valid_df_cols]
    
    def compute_mse(self, real, pred, axis=None):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지로 MSE 계산.
        Args:
            real: numpy array, ticket_time로 추론한 시간대의 실제 위성 이미지들
            pred: numpy array, ticket_time로 추론한 시간대의 예측 위성 이미지들
            axis: int or tuple, np.mean 에 적용할 axes
        Returns:
            MSE: float or list, MSE 결과
        """
        MSE = np.square(np.subtract(real, pred)).mean(axis=axis)
        return np.round_(MSE, self.round_num)

    def compute_mae(self, real, pred, axis=None):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지로 MAE 계산.
        Args:
            real: numpy array, ticket_time로 추론한 시간대의 실제 위성 이미지들
            pred: numpy array, ticket_time로 추론한 시간대의 예측 위성 이미지들
            axis: int or tuple, np.mean 에 적용할 axes
        Returns:
            MAE: float or list, MAE 결과
        """
        MAE = np.subtract(real, pred).mean(axis=axis)
        return np.round_(MAE, self.round_num)
    
    def save_valid_result(self,
                          valid_df_list,
                          ticket_time_list,
                          kst_start_time,
                          kst_end_time):
        """
        Description:
            검증결과로 csv로 저장.
        Args:
            valid_df_list: list, 전체 검증결과 list
            ticket_time_list: list, ticket에 해당하는 시간대 리스트 (UTC)
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        self.log.info(">>> Start to Save Validations")
        start_time = time.time()
        for valid_df, ticket_time in zip(valid_df_list, ticket_time_list):
            saved_dir = return_dir(True,
                                   self.valid_chn_dir,
                                   ticket_time[:4],   # year
                                   ticket_time[4:6],   # month
                                   ticket_time[6:8])   # day
            save_file = self.saved_file.format(self.chn, ticket_time)
            save_path = os.path.join(saved_dir, save_file)
            valid_df.to_csv(save_path, encoding="utf-8", index=False)
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Save Validations: {0}(KST) ~ {1}(KST)".format(kst_start_time, kst_end_time))
        self.log.info(">>> End to Save Validations: {0}(UTC) ~ {1}(UTC)".format(ticket_time_list[0], ticket_time_list[-1]))