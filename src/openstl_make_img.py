import os
import time
import numpy as np
import cv2

from src.extract_data import GK2ADataExtractor
from src.sat_calibration import SatCalibration
from utils.code_utils import return_dir
from utils.log_module import print_elapsed_time


class SatImg(object):
    """Make Black-and-White Image using Satellite prediction."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상으로 흑백 이미지 산출 및 저장.
            원본과 모델 추론 결과 각각 처리.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatImg, self).__init__()
        cfg_d = cfg.PARAMS.DATA
        cfg_r = cfg.RAWDATA
        cfg_i = cfg.INFER
        
        self.chn = cfg_d.CHANNEL
        self.chn_min = getattr(cfg.PARAMS.NORMALIZER, self.chn.upper()).MIN_VALUE
        self.chn_max = getattr(cfg.PARAMS.NORMALIZER, self.chn.upper()).MAX_VALUE
        self.h, self.w, _ = cfg_d.INPUT_SHAPE
        self.output_seq = cfg_d.OUTPUT_SEQ
        self.time_step = cfg_r.TIME_STEP
        self.infer_loop = cfg_i.LOOP
        self.output_dir = cfg_i.OUTPUT_DIR
        self.output_file = cfg_i.OUTPUT_FILE
        self.saved_img = cfg_i.OUTPUT_IMG
        self.making_real = cfg_i.MAKING_REAL
        
        self.result_seq = self.output_seq * self.infer_loop
        self.result_chn_dir = return_dir(False,
                                         self.output_dir,
                                         self.chn)
        self.img_chn_dir = return_dir(False,
                                      self.output_dir,
                                      "{0}_img".format(self.chn))
        
        # Additional
        self.sat = GK2ADataExtractor(cfg, log, err_log, pred_mode=True)
        calib = SatCalibration(cfg, log, err_log)
                
        self.calibration_table = calib.make_calibration_table()
        
        latlon = calib.load_latlon()
        self.effective_area_pixel = calib.make_effective_area_pixel_outline(latlon)
        lat_pix_min, lat_pix_max, lon_pix_min, lon_pix_max = self.effective_area_pixel
        self.data_h = lat_pix_max - lat_pix_min
        self.data_w = lon_pix_max - lon_pix_min
        
        self.log, self.err_log = log, err_log

    def __call__(self, kst_start_time, kst_end_time):
        """
        Description:
            원본 위성 이미지 및 추론 결과 이미지를 흑백 이미지(.png)로 저장.
            kst_start_time ~ kst_end_time 사이 시간을 ticket으로 사용.
            파일명은 UTC를 사용.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        self.log.info(">>> Start to Make Satellite images")
        start_time = time.time()

        img_upper_dir = self.img_chn_dir
        TICKET_LEN = self._ticket_time_len(kst_start_time, kst_end_time)
        
        # 실제 위성 이미지 (Calibration table 적용)
        if self.making_real:
            self.log.info(">>> Load & Save Real Satellite images")
            real_sat_array, utc_time_list = self.load_real_sat(kst_start_time,
                                                               kst_end_time)
            if isinstance(self.calibration_table, np.ndarray):
                real_sat_array = self.calibration_table[real_sat_array]
            real_sat_norm = self.convert_to_img(real_sat_array,
                                                self.chn_min,
                                                self.chn_max)
            self.save_all_imgs(real_sat_norm,
                               utc_time_list,
                               img_upper_dir,
                               is_real=True)
            self.log.info(">>> Load & Save Real Satellite images completely")
        else:
            utc_time_list = self.load_real_sat(kst_start_time,
                                               kst_end_time)
        
        # 추론 위성 이미지 결과
        self.log.info(">>> Load & Save Predicted Satellite images")
        for idx in range(TICKET_LEN):
            ticket_utc_time = utc_time_list[idx]
            pred_utc_time_list = utc_time_list[idx+1:idx+1+self.result_seq]
            self.log.info(">>> Ticket UTC time: {0}".format(ticket_utc_time))
            pred_sat_array = self.load_pred_sat(ticket_utc_time,
                                                pred_utc_time_list)
            if isinstance(self.calibration_table, np.ndarray):
                pred_sat_array = self.calibration_table[pred_sat_array]
            pred_sat_norm = self.convert_to_img(pred_sat_array,
                                                self.chn_min,
                                                self.chn_max)
            self.save_all_imgs(pred_sat_norm,
                               pred_utc_time_list,
                               img_upper_dir,
                               is_real=False,
                               ticket_time=ticket_utc_time)
        self.log.info(">>> Load & Save Predicted Satellite images completely")
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Make Satellite images")

    def _ticket_time_len(self, kst_start_time, kst_end_time):
        """
        Description:
            입력된 시간대 데이터 수.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            TICKET_LEN: int, kst_start_time ~ kst_end_time 시간대 리스트 길이
        """
        ticket_time_list = self.sat._make_time_list(kst_start_time,
                                                    kst_end_time,
                                                    "min",
                                                    self.time_step)
        return len(ticket_time_list)

    def load_real_sat(self, kst_start_time, kst_end_time):
        """
        Description:
            원본 위성 영상 데이터 불러오기.
            추론 시간대
                - kst_start_time 보다 1 step 뒤부터
                - kst_end_time 보다 time_step * output_seq * infer_loop step 뒤까지
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            sat_array: numpy array, 전체 위성 배열
            utc_time_list: list, UTC로 변환한 시간대 리스트, sat_array보다 길이가 1 길음
        """
        first_kst_time = kst_start_time
        # 추론결과 맨처음
        kst_start_time = self.sat._compute_date(kst_start_time,
                                                "min",
                                                self.time_step)
        # 추론결과 맨마지막
        kst_end_time = self.sat._compute_date(kst_end_time,
                                              "min",
                                              self.time_step * self.result_seq)
        # KST -> UTC
        utc_start_time = self.sat._kst_to_utc(first_kst_time)
        utc_end_time = self.sat._kst_to_utc(kst_end_time)
        utc_time_list = self.sat._make_time_list(utc_start_time,
                                                 utc_end_time,
                                                 "min",
                                                 self.time_step)
        if self.making_real:
            kst_time_list = self.sat._make_time_list(kst_start_time,
                                                     kst_end_time,
                                                     "min",
                                                     self.time_step)
            self.log.info(">>> Using Channel / Init date(KST) / End date(KST): {0} / {1} / {2}".format(self.chn,
                                                                                                       kst_time_list[0],
                                                                                                       kst_time_list[-1]))
            self.log.info(">>> Using Channel / Init date(UTC) / End date(UTC): {0} / {1} / {2}".format(self.chn,
                                                                                                       self.sat._kst_to_utc(kst_time_list[0]),
                                                                                                       self.sat._kst_to_utc(kst_time_list[-1])))
            self.log.info(">>> Time counts: {0}".format(len(kst_time_list)))
            sat_array = self.sat._make_sat_ary(kst_time_list, True)
            return sat_array, utc_time_list
        else:
            return utc_time_list

    def load_pred_sat(self, ticket_utc_time, pred_utc_time_list):
        """
        Description:
            추론 결과 위성 영상 데이터 불러오기.
            ticket_utc_time을 기준으로 pred_utc_time_list 추론 결과 불러오기.
        Args:
            ticket_utc_time: str, 추론 시점 UTC 연월일시분(12자리)
            pred_utc_time_list: list, ticket으로 추론한 시간대 리스트(output_seq * loop 길이)
        Returns:
            pred_sat_array: numpy array, 위성 배열
        """
        pred_sat_array = np.empty((len(pred_utc_time_list), self.data_h, self.data_w),
                                  dtype=np.uint16)
        result_dir = return_dir(False,
                                self.result_chn_dir,
                                ticket_utc_time[:4],   # year
                                ticket_utc_time[4:6],   # month
                                ticket_utc_time[6:8],   # day
                                ticket_utc_time)
        # npy 불러온 뒤 (h, w, 1)를 (h, w) 로 변환(squeeze)
        for idx, utc_time in enumerate(pred_utc_time_list):
            result_path = os.path.join(result_dir,
                                       self.output_file.format(self.chn, utc_time))
            sat_array = np.load(result_path)
            sat_array = np.squeeze(sat_array, axis=-1)
            pred_sat_array[idx] = sat_array
        return pred_sat_array

    def convert_to_img(self, sat_array, chn_min, chn_max):
        """
        Description:
            위성 배열을 흑백 이미지로 변환.
            이미지 색상 반전되지 않도록 추가 조정
                - VI 및 NR Calibration 전 & 이외 채널 Calibration 후
                - VI 및 NR Calibration 후 & 이외 채널 Calibration 전
        Args:
            sat_array: numpy array, 위성 배열
            chn_min: MinMax 할 때의 채널의 최솟값
            chn_max: MinMax 할 때의 채널의 최댓값
        Returns:
            sat_norm: numpy array, uint8로 변환된 위성 배열
        """
        sat_norm = (sat_array - chn_min) / (chn_max - chn_min) * 255
        sat_norm = np.clip(sat_norm, 0, 255)
        sat_norm = sat_norm.astype(np.uint8)
        chn_bool = self.chn.startswith("vi") or \
                   self.chn.startswith("nr") or \
                   self.chn.startswith("sw") or \
                   self.chn.startswith("ir112")
        if chn_bool:
            pass
        else:
            sat_norm = 255 - sat_norm
        return sat_norm

    def save_all_imgs(self,
                      sat_norm,
                      utc_time_list,
                      img_upper_dir,
                      is_real,
                      ticket_time=None):
        """
        Description:
            흑백 이미지로 변환한 위성 배열 전체 저장.
        Args:
            sat_norm: numpy array, uint8로 변환된 위성 배열
            utc_time_list: list, UTC로 변환한 시간대 리스트
                           실제: sat_array보다 길이가 1 길음
                           추론결과: output_seq * loop 길이
            img_upper_dir: str, 이미지 저장할 상위 디렉토리
            is_real: bool, 실제인지 추론 결과인지 판단
            ticket_time: str, 추론 결과일 때, 추론 시점 UTC 연월일시분(12자리)
        Returns:
            None
        """
        if is_real:
            log_str = "real"
            utc_time_list = utc_time_list[1:]   # 맨 앞은 추론 이미지 X
        else:
            log_str = "pred"

        for idx, utc_time in enumerate(utc_time_list):
            self.save_one_img(sat_norm[idx],
                              utc_time,
                              img_upper_dir,
                              is_real,
                              ticket_time)
        kst_start_time = self.sat._utc_to_kst(utc_time_list[0])
        kst_end_time = self.sat._utc_to_kst(utc_time_list[-1])
        self.log.info("       Save {0} {1} imgs: {2}(KST) ~ {3}(KST)".format(log_str, self.chn, kst_start_time, kst_end_time))
        self.log.info("       Save {0} {1} imgs: {2}(UTC) ~ {3}(UTC)".format(log_str, self.chn, utc_time_list[0], utc_time_list[-1]))

    def save_one_img(self,
                     one_sat_norm,
                     utc_time,
                     img_upper_dir,
                     is_real,
                     ticket_time):
        """
        Description:
            흑백 이미지로 변환한 위성 배열 개별 저장.
        Args:
            one_sat_norm: numpy array, uint8로 변환된 위성 배열(1개 시점)
            utc_time: str, UTC 연월일시분(12자리)
            img_upper_dir: str, 이미지 저장할 상위 디렉토리
            is_real: bool, 실제인지 추론 결과인지 판단
            ticket_time: str, 추론 결과일 때, 추론 시점 UTC 연월일시분(12자리)
        Returns:
            None
        """
        if is_real:
            child_dir = "_real"
            dir_time = utc_time
        else:
            child_dir = ticket_time
            dir_time = ticket_time
        img_saved_dir = return_dir(True,
                                   img_upper_dir,
                                   dir_time[:4],   # year
                                   dir_time[4:6],   # month
                                   dir_time[6:8],   # day
                                   child_dir)
        img_name = self.saved_img.format(self.chn, utc_time)
        img_path = os.path.join(img_saved_dir, img_name)
        cv2.imwrite(img_path, one_sat_norm)
