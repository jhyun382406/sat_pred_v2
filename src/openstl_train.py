import time

from utils.log_module import print_elapsed_time
from openstl.api import BaseExperiment
from openstl.utils import (
    setup_multi_processes, get_dist_info
)


class SatTrain(object):
    """Train using Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 학습 init 설정.
            모델 학습.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatTrain, self).__init__()
        self.chn = cfg.PARAMS.DATA.CHANNEL

        self.log, self.err_log = log, err_log
        
    def __call__(self,
                 args,
                 train_dl,
                 valid_dl,
                 test_dl,
                 prep,
                 mean_std=None
                 ):
        """
        Description:
            모델 학습.
            OpenSTL 인스턴스 생성 및 학습.
            Dataloader는 학습/추론 다르게 적용
                - 학습: train / valid / valid
                - 추론: test / test / test
            저장경로로 인해 평균/표준편차 저장도 진행.
        Args:
            args: namedtuple, OpenSTL 설정
            train_dl: torch.Dataloader, 학습 데이터로더
            valid_dl: torch.Dataloader, 검증 데이터로더
            test_dl: torch.Dataloader, 테스트 데이터로더
            prep: SatPrep, 전처리 클래스 인스턴스
            mean_std: tuple, (채널값 평균 (T, H, W), 채널값 표준편차 (T, H, W))
        """
        # set multi-process settings
        setup_multi_processes(args.__dict__)
        
        # 기저 인스턴스 생성
        openstl = BaseExperiment(
            args,
            dataloaders=(train_dl, valid_dl, test_dl)
        )
        self.log.info("Setting OpenSTL instance")
        
        rank, _ = get_dist_info()
        self.log.info("OpenSTL's rank settings: {0}".format(rank))
        
        # Mean, Std 저장
        if not mean_std is None:
            sat_mean, sat_std = mean_std
            prep.save_mean_std(openstl.path, sat_mean, sat_std)
            self.log.info("Save {0} Pixelwise Mean & Standard deviation".format(self.chn))
        
        self.log.info(">>> Model & Applied Settings path: {0}".format(openstl.path))
        self.log.info(">>> Start Training")
        start_time = time.time()
        
        # 학습
        ##################### Check dataloader (num_workers > 0)
        # import torch
        # s_time = time.time()
        # for idx, data in enumerate(train_dl):
        #     x, y = data[0], data[1]
        #     if idx % 10 == 0 or idx == len(train_dl)-1:
        #         self.log.info("{0} ::: {1} ({2}) / {3} ({4})".format(idx, x.shape, type(x), y.shape, type(y)))
        #     h, m, s = print_elapsed_time(s_time)
        #     # del x, y
        #     torch.cuda.empty_cache()
        #     if idx % 10 == 0 or idx == len(train_dl)-1:
        #         self.log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        #     s_time = time.time()
        #####################
        openstl.train()
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End Training")
