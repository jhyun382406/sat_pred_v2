import numpy as np


def min_max_normalizer(
    array: np.memmap=None,
    min_value: float=None,
    max_value: float=None
    ):
    array = np.clip(array, min_value, max_value)
    return (array - min_value) / (max_value - min_value)


def inv_min_max_normalizer(
    array: np.memmap=None,
    min_value: float=None,
    max_value: float=None
    ):
    return min_value + (max_value - min_value) * array


def gaussian_normalizer(
    array=None,
    mean=None,
    std=None
    ):
    """
    Description:
        Gaussian Normalizer.
    Args:
        array: numpy memmap or dask array, 배열
        mean: float or numpy array, 평균
        std: float or numpy array, 표준편차
    Returns:
        norm_array: numpy memmap or dask array, Normalize된 배열
    """
    return (array - mean) / std


def inv_gaussian_normalizer(
    array=None,
    mean=None,
    std=None
    ):
    """
    Description:
        Gaussian Normalizer inverse.
    Args:
        array: numpy memmap or dask array, 배열
        mean: float or numpy array, 평균
        std: float or numpy array, 표준편차
    Returns:
        norm_array: numpy memmap or dask array, Inverse Normalize된 배열
    """
    return mean + array * std