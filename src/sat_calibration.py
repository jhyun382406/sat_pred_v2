import os
from datetime import timezone, timedelta

import numpy as np
import pandas as pd
import ray
from pysolar.solar import get_altitude


class SatCalibration(object):
    """Calibrate Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 Calibration init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatCalibration, self).__init__()
        # Data
        cfg_d = cfg.PARAMS.DATA
        cfg_rw = cfg.RAWDATA
        self.chn = cfg_d.CHANNEL
        self.in_seq = cfg_d.INPUT_SEQ
        self.out_seq = cfg_d.OUTPUT_SEQ

        # Calibrate
        self.TZ_KST = timezone(timedelta(hours=9))
        self.use_calibration = cfg_d.CALIBRATION.USE
        self.use_cal_col = cfg_d.CALIBRATION.USE_COL
        self.calibration_dir = cfg_rw.CALIBRATION_DIR
        self.cal_table_file = cfg_rw.CAL_TABLE_FILE
        self.cal_table_dtype = getattr(np, cfg_rw.CAL_TABLE_DTYPE)
        self.use_sza = cfg_d.SZA.USE
        self.latlon_file = cfg_rw.LATLON_FILE
        self.h, self.w = cfg_rw.LATALON_RESOLUTION
        self.use_ray = cfg_d.SZA.USE_RAY
        self.ray_cpus = cfg_d.SZA.RAY_CPUS
        self.sza_interval = cfg_d.SZA.SZA_INTERVAL
        self.west_latlon = cfg_d.SZA.WEST_LATLON
        self.east_latlon = cfg_d.SZA.EAST_LATLON
        self.day_bool = cfg_d.SZA.DAY_BOOL
        self.night_bool = cfg_d.SZA.NIGHT_BOOL
        self.dawn_twil_bool = cfg_d.SZA.DAWN_TWIL_BOOL
        self.use_crop_area = cfg_d.CROP_AREA.USE
        self.area_center_lat = cfg_d.CROP_AREA.CENTER_LAT
        self.area_center_lon = cfg_d.CROP_AREA.CENTER_LON
        self.area_north_lat = cfg_d.PRED_CROP_AREA.NORTH_LAT
        self.area_south_lat = cfg_d.PRED_CROP_AREA.SOUTH_LAT
        self.area_west_lon = cfg_d.PRED_CROP_AREA.WEST_LON
        self.area_east_lon = cfg_d.PRED_CROP_AREA.EAST_LON
        self.area_h = cfg_d.INPUT_SHAPE[0]
        self.area_w = cfg_d.INPUT_SHAPE[1]
        self.area_half_h = cfg_d.INPUT_SHAPE[0] // 2
        self.area_half_w = cfg_d.INPUT_SHAPE[1] // 2

        self.log, self.err_log = log, err_log
        
    def __call__(self):
        """
        Description:
            None
        Args:
            None
        Returns:
            None
        """
        pass
    
    def make_calibration_table(self):
        """
        Description:
            cfg에 설정한 채널의 Calibration table.
        Args:
            None
        Returns:
            calibration_table: numpy array, 해당 채널의 Calibration table
        """
        if self.use_calibration:
            calibration_table = self.load_calibration_table()
            self.log.info("Use Calibration table: {0}".format(self.use_cal_col))
        else:
            calibration_table = None
            self.log.info("Use Raw data: {0}".format(self.chn))
        return calibration_table
    
    def ray_start(self):
        """
        Description:
            Ray 시작.
        """
        if not ray.is_initialized() and self.use_ray:
            ray.init(num_cpus=self.ray_cpus)
            self.log.info("Ray start")
    
    def ray_stop(self):
        """
        Description:
            Ray 종료.
        """
        if ray.is_initialized() and self.use_ray:
            ray.shutdown()
            self.log.info("Ray stop")
    
    def setting_sza(self, latlon):
        """
        Description:
            태양천정각 계산 준비.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
        """
        if self.use_sza:
            west_pixel = self.get_criteria_pixel(latlon, self.west_latlon)
            east_pixel = self.get_criteria_pixel(latlon, self.east_latlon)
            return west_pixel, east_pixel
        else:
            return None, None
    
    def make_seq_indices(self,
                         latlon,
                         west_pixel,
                         east_pixel,
                         kst_obj_list,
                         total_seq):
        """
        Description:
            태양천정각 계산 및 사용할 인덱스 추출 (Sequence 반영).
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
            kst_obj_list: list, KST datetime object의 리스트
            day_bool: bool, 주간 시간대 결과 산출 여부
            night_bool: bool, 야간 시간대 결과 산출 여부
            dawn_twil_bool: bool, 여명/황혼 시간대 결과 산출 여부
            total_seq: int, Sequence 전체 길이 (학습은 input+output 길이, 추론은 input 길이)
        Returns:
            seq_indices: list, Sequence 시작 index
        """
        if self.use_sza:
            sza = self.make_sza(latlon, west_pixel, east_pixel, kst_obj_list)
            day_indices, night_indices, dawn_twil_indices = self.judge_indices(sza)
            use_indices = self.make_use_indices(day_indices, night_indices, dawn_twil_indices)
        else:
            use_indices = [i for i in range(len(kst_obj_list))]
        seq_indices = self.extract_seq_indices(use_indices, total_seq)
        self.log.info("Extract Sequences' indices")
        return seq_indices

    def load_calibration_table(self):
        """
        Description:
            GK2A Calibration table 불러오기.
            가시 ~ 근적외: Radiance, Albedo
            단파적외 ~ 적외: Radiance, Brightness Temperature
        Args:
            None
        Returns:
            calibration_table: numpy array, 채널의 Calibration table
        """
        calibration_table_path = os.path.join(
            self.calibration_dir,
            self.cal_table_file
        )
        try:
            calibration_table = pd.read_csv(
                calibration_table_path,
                encoding="utf-8",
                dtype=self.cal_table_dtype
            )
        except Exception as e:
            self.log.info("Load csv file incompletely: {0}".format(self.cal_table_file))
            self.err_log.error("Load csv file incompletely: {0}".format(self.cal_table_file))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            calibration_table = calibration_table[self.use_cal_col].to_numpy()
            return calibration_table

    def load_latlon(self):
        """
        Description:
            위/경도 파일 불러오기.
        Args:
            None
        Returns:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
                - 500m: (3600, 3600, 2)
                - 1km: (1800, 1800, 2)
                - 2km: (900, 900, 2)
        """
        latlon_path = os.path.join(self.calibration_dir, self.latlon_file)
        try:
            latlon = np.loadtxt(latlon_path, delimiter="\t", dtype=float)
            latlon = latlon.reshape(self.h, self.w, -1)
        except Exception as e:
            self.log.info("Load txt file incompletely: {0}".format(self.latlon_file))
            self.err_log.error("Load txt file incompletely: {0}".format(self.latlon_file))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            self.log.info("Latitude & Longitude Resolution: {0}".format(latlon.shape))
            return latlon
    
    def get_criteria_pixel(self, latlon, criteria_latlon):
        """
        Description:
            태양천정각으로 주간, 야간, 여명/황혼 구분하는 기준위치의 픽셀 찾기.
            단순히 픽셀 L2 길이 최소인 픽셀로 반환.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            criteria_latlon: list, 구분 기준 위/경도 (위도, 경도)
        Returns:
            pixel: tuple, 구분 기준 픽셀 (위도, 경도)
        """
        criteria_ary = np.tile(criteria_latlon, reps=[self.h, self.w, 1])
        l2 = np.linalg.norm(latlon - criteria_ary, axis=-1)
        pixel = np.unravel_index(l2.argmin(), l2.shape)
        return pixel
    
    def make_sza(self, latlon, west_pixel, east_pixel, kst_obj_list):
        """
        Description:
            태양천정각(SZA) 계산.
            Ray 사용여부 flag가 있음.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
            kst_obj_list: list, KST datetime object의 리스트
        Returns:
            sza: numpy array, 입력된 KST 시간에 따른 기준 픽셀 태양천정각
        """
        if self.use_ray:
            west_latlon = ray.put(latlon[west_pixel])
            east_latlon = ray.put(latlon[east_pixel])
            sza_ids = [
                # @ray.remote에는 self도 argument로 넣어줘야 함
                self.ray_make_sza_criteria.remote(self, west_latlon, east_latlon, kst_obj)
                for kst_obj
                in kst_obj_list
            ]
            sza = np.array(ray.get(sza_ids), dtype=np.float32)
        else:
            west_latlon = latlon[west_pixel]
            east_latlon = latlon[east_pixel]
            sza = np.empty((len(kst_obj_list), 2), dtype=np.float32)
            for idx, kst_obj in enumerate(kst_obj_list):
                sza[idx] = self.make_sza_criteria(west_latlon, east_latlon, kst_obj)
        return sza

    def compute_sza(self, lat, lon, kst_obj):
        """
        Description:
            태양천정각(SZA) = 90 - 태양고도.
        Args:
            lat: float or numpy array, 위도
            lon: float or numpy array, 경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            sza: float or numpy array, 태양천정각
        """
        return 90 - get_altitude(lat, lon, kst_obj)
    
    def make_sza_criteria(self, west_latlon, east_latlon, kst_obj):
        """
        Description:
            동서쪽 기준위치의 태양천정각.
        Args:
            west_latlon: numpy array, 구분 기준 서쪽 위/경도
            east_latlon: numpy array, 구분 기준 동쪽 위/경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            one_time_sza: numpy array, 해당 KST 시각의 태양천정각
        """
        west_sza = self.compute_sza(west_latlon[0], west_latlon[1], kst_obj)
        east_sza = self.compute_sza(east_latlon[0], east_latlon[1], kst_obj)
        return np.array([west_sza, east_sza])
    
    @ray.remote
    def ray_make_sza_criteria(self, west_latlon, east_latlon, kst_obj):
        """
        Description:
            동서쪽 기준위치의 태양천정각 (Ray 사용).
            Ray는 독립적인 함수나 Class에 적용하는 것을 권장.
        Args:
            west_latlon: numpy array, 구분 기준 서쪽 위/경도
            east_latlon: numpy array, 구분 기준 동쪽 위/경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            one_time_sza: list, 해당 KST 시각의 태양천정각
        """
        west_sza = self.compute_sza(west_latlon[0], west_latlon[1], kst_obj)
        east_sza = self.compute_sza(east_latlon[0], east_latlon[1], kst_obj)
        return [west_sza, east_sza]

    def judge_indices(self, sza):
        """
        Description:
            태양천정각(SZA)을 이용하여 주간, 야간, 여명/황혼 분리.
                - 주간: SZA < 80
                - 야간: SZA >= 88
                - 여명/황혼: 80 <= SZA < 88
            각각에 해당하는 index들만 추출.
            Ray 사용여부 flag가 있음.
        Args:
            sza: numpy array, 시간에 따른 기준 픽셀 태양천정각
        Returns:
            day_indices: list, 주간 시간대 index
            night_indices: list, 야간 시간대 index
            dawn_twil_indices: list, 여명/황혼 시간대 index
        """
        day_bool = sza[:, 0] < self.sza_interval[0]   # Day
        night_bool = sza[:, 1] >= self.sza_interval[1]   # Night
        dawn_twil_bool = ~np.logical_or(day_bool, night_bool)   # Dawn / Twilight
        day_indices = np.where(day_bool == True)[0]
        night_indices = np.where(night_bool == True)[0]
        dawn_twil_indices = np.where(dawn_twil_bool == True)[0]
        return day_indices, night_indices, dawn_twil_indices
    
    def make_use_indices(self, day_indices, night_indices, dawn_twil_indices):
        """
        Description:
            사용할 index를 순서대로 정리.
            분리한 주간, 야간, 여명/황혼에서 사용할 시간대를 합침.
        Args:
            day_indices: list, 주간 시간대 index
            night_indices: list, 야간 시간대 index
            dawn_twil_indices: list, 여명/황혼 시간대 index
        Returns:
            use_indices: list, 사용할 시간대 index
        """
        use_indices = list()
        if self.day_bool:
            use_indices.extend(day_indices)
            self.log.info("Contain Day times indices")
        if self.night_bool:
            use_indices.extend(night_indices)
            self.log.info("Contain Night times indices")
        if self.dawn_twil_bool:
            use_indices.extend(dawn_twil_indices)
            self.log.info("Contain Dawn/Twilight times indices")
        use_indices = sorted(use_indices)
        return use_indices

    def extract_seq_indices(self, idxes, total_seq):
        """
        Description:
            Sequence 생성할 수 있는 시작 index만 추출.
                - step_indices 예시
                  [[    0,     1, ...,    22,    23],
                   [    0,     1, ...,    22,    23],
                   ...,
                   [    0,     1, ...,    22,    23],
                   [    0,     1, ...,    22,    23]]
                - start_indices 예시
                  [[   50,    50, ...,    50,    50],
                   [   51,    51, ...,    51,    51],
                   ...,
                   [13216, 13216, ..., 13216, 13216],
                   [13217, 13217, ..., 13217, 13217]]
        Args:
            idxes: list, 사용할 시간대 index
            total_seq: int, Sequence 전체 길이 (학습은 input+output 길이, 추론은 input 길이)
        Returns:
            seq_indices: list, sequence 생성 가능한 시간대 index
        """
        # 차이가 1인 index 여야 연속된 Sequence 생성 가능
        idxes = np.array(idxes)
        idx_diff = idxes[1:] - idxes[:-1]
        continuous_indices = idxes[np.where(idx_diff == 1)[0]]
        
        # 차이가 1인 index 와 계산하기 위한 array 준비
        step_indices = np.tile(np.arange(total_seq),
                             reps=[continuous_indices.shape[0], 1])
        start_indices = np.tile(continuous_indices,
                              reps=[total_seq, 1]).T
        all_seq_indices = step_indices + start_indices

        # True 개수가 학습 in/out Sequence 길이가 같으면 Sequence 생성 가능
        seq_indices = list()
        for seq_idx in all_seq_indices:
            if np.isin(continuous_indices, seq_idx).sum() == total_seq:
                seq_indices.append(seq_idx[0])
        
        return seq_indices

    def make_effective_area_pixel_center(self, latlon):
        """
        Description:
            위/경도로 해당 해상도의 픽셀 위치 계산.
            가운데 위경도를 기준으로 정사각 영역 픽셀 계산.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        """
        if self.use_crop_area:
            # 위/경도 -> 픽셀
            center_pixel = self._latlon_to_pixel(self.area_center_lat, self.area_center_lon, latlon)
            # 직사각형 영역 계산
            lat_pixel_min = center_pixel[0] - self.area_half_h
            lat_pixel_max = center_pixel[0] + self.area_half_h
            lon_pixel_min = center_pixel[1] - self.area_half_w
            lon_pixel_max = center_pixel[1] + self.area_half_w
            effective_area_pixel = (lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max)
        else:
            effective_area_pixel = None
        return effective_area_pixel
    
    def make_effective_area_pixel_outline(self, latlon):
        """
        Description:
            위/경도로 해당 해상도의 픽셀 위치 계산.
            계산한 사각형 영역을 직사각형 영역으로 최종 변경.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        """
        if self.use_crop_area:
            # 위/경도 -> 픽셀
            nw_pixel = self._latlon_to_pixel(self.area_north_lat, self.area_west_lon, latlon)
            ne_pixel = self._latlon_to_pixel(self.area_north_lat, self.area_east_lon, latlon)
            sw_pixel = self._latlon_to_pixel(self.area_south_lat, self.area_west_lon, latlon)
            se_pixel = self._latlon_to_pixel(self.area_south_lat, self.area_east_lon, latlon)
            # 직사각형 영역 계산
            pixel_ary = np.array((nw_pixel, ne_pixel, sw_pixel, se_pixel))
            lat_pixel_min = pixel_ary[:, 0].min()
            lat_pixel_max = pixel_ary[:, 0].max()
            lon_pixel_min = pixel_ary[:, 1].min()
            lon_pixel_max = pixel_ary[:, 1].max()
            # Input shape보다 작은 경우 크기 보정
            lat_pixel_min, lat_pixel_max = self._correct_pix(lat_pixel_min,
                                                             lat_pixel_max,
                                                             self.area_h)
            lon_pixel_min, lon_pixel_max = self._correct_pix(lon_pixel_min,
                                                             lon_pixel_max,
                                                             self.area_w)
            effective_area_pixel = (lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max)
        else:
            effective_area_pixel = None
        return effective_area_pixel
    
    def _correct_pix(self, min_value, max_value, standard):
        """
        Description:
            input shape보다 area가 작을 경우 크기 보정.
                - 좌측으로 연장하는걸 기준으로 진행
                - 음수가 나오면 오른쪽으로도 추가 보정
        Args:
            min_value: int, 픽셀값 최소
            max_value: int, 픽셀값 최대
            standard: int, 기준 크기
        Returns:
            min_value: int, 보정된 픽셀값 최소
            max_value: int, 보정된 픽셀값 최대
        """
        crop_len = max_value - min_value
        if crop_len < standard:
            correct_left_pix = standard - crop_len
            min_value -= correct_left_pix
            if min_value < 0:
                correct_right_pix = abs(min_value)
                min_value = 0
                max_value += correct_right_pix
        return min_value, max_value

    def _latlon_to_pixel(self, lat, lon, latlon):
        """
        Description:
            위/경도로 해당 해상도의 픽셀 위치 계산.
            L2 norm이 가장 작은 위치 픽셀을 반환.
        Args:
            lat: float, 위도
            lon: float, 경도
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            latlon_pixel: tuple, 위/경도 픽셀, (위도, 경도)
        """
        near_latlon = latlon[
            (latlon[..., 0] > lat - 0.1) &
            (latlon[..., 0] < lat + 0.1) &
            (latlon[..., 1] > lon - 0.1) &
            (latlon[..., 1] < lon + 0.1)
        ]
        latlon_tile = np.tile((lat, lon), reps=[near_latlon.shape[0], 1])
        latlon_l2 = np.linalg.norm(near_latlon - latlon_tile, axis=-1)
        lat_adapt, lon_adapt = near_latlon[latlon_l2.argmin()]
        lat_pix, lon_pix = np.where(
            (latlon[..., 0] == lat_adapt) &
            (latlon[..., 1] == lon_adapt)
        )   # 각각 array로 결과 산출됨
        return (lat_pix[0], lon_pix[0])

    def remove_unused_indices(self, origin_indices, rm_indices):
        """
        Description:
            불필요 index를 제외.
        Args:
            origin_indices: list, 기준 index 리스트
            rm_indices: list, 제외할 index 리스트
        Returns:
            filtered_indices: list, 제외 처리 완료한 index 리스트
        """
        return [elm for elm in origin_indices if not elm in rm_indices]

    def convert_alb2raw(self, sat_array, chn_cal_coeff):
        """
        Description:
            GK2A Calibration radiance, albedo 결과로부터 원래 위성 raw 값 계산.
            가시 ~ 근적외 채널에 해당.
            Albedo -> Radiance -> Raw
        Args:
            sat_array: numpy array, 모델 결과 역변환까지 완료한 데이터
            chn_cal_coeff: namedtuple, 채널의 Calibration coefficient
        Returns:
            sat_array: numpy array, 위성 raw  변환 완료한 데이터
        """
        wv = chn_cal_coeff.CENTER_WAVE_LENGTH
        gain = chn_cal_coeff.DN2RAD_GAIN
        offset = chn_cal_coeff.DN2RAD_OFFSET
        rad2alb = chn_cal_coeff.RAD2ALB
        raw_max = chn_cal_coeff.RAW_MAX
        
        if self.use_cal_col.endswith("radiance"):
            sat_array = np.round((sat_array - offset) / gain)
        elif self.use_cal_col.endswith("albedo"):
            sat_array = sat_array / rad2alb
            sat_array = np.round((sat_array - offset) / gain)
        else:
            self.log.info("Cannot convert satellite data: {0}".format(self.use_cal_col))
            self.log.info("Use right postfix: radiance, albedo")
            self.err_log.error("Cannot convert satellite data: {0}".format(self.use_cal_col))
        
        # Raw와 같은 data type으로 변환
        sat_array = np.clip(sat_array, 0, raw_max)
        sat_array = sat_array.astype(getattr(np, self.raw_dtype))
        
        return sat_array
    
    def convert_bt2raw(self, sat_array, const_cal_coeff, chn_cal_coeff):
        """
        Description:
            GK2A Calibration 결과로부터 원래 위성 raw 값 계산.
            단파적외 ~ 적외 채널에 해당.
            Brightness Temperature (BT) -> Effective BT -> Radiance -> Raw
        Args:
            sat_array: numpy array, 
            const_cal_coeff: namedtuple, Calibration 공용 상수
            chn_cal_coeff: namedtuple, 채널의 Calibration coefficient
        Returns:
            sat_array: numpy array, 위성 raw  변환 완료한 데이터
        """
        c = const_cal_coeff.LIGHT_SPEED
        h = const_cal_coeff.PLANCK
        k = const_cal_coeff.BOLTZMANN
        wv = chn_cal_coeff.CENTER_WAVE_NUM
        gain = chn_cal_coeff.DN2RAD_GAIN
        offset = chn_cal_coeff.DN2RAD_OFFSET
        c0 = chn_cal_coeff.C0
        c1 = chn_cal_coeff.C1
        c2 = chn_cal_coeff.C2
        raw_max = chn_cal_coeff.RAW_MAX
        
        _hc_k = h*c/k
        _2hc2 = 2*h*(c**2)
        wv100 = wv * 100
        
        if self.use_cal_col.endswith("radiance"):
            sat_array = np.round((sat_array - offset) / gain)
        elif self.use_cal_col.endswith("bt"):
            sat_array = (-c1 + np.sqrt(c1 ** 2 - 4 * c2 * (c0 - sat_array))) / (2 * c2)
            sat_array = (_2hc2 * wv100**3) / ( (np.exp(_hc_k * wv100 / sat_array) - 1) * 10**(-5) )
            sat_array = np.round((sat_array - offset) / gain)
        else:
            self.log.info("Cannot convert satellite data: {0}".format(self.use_cal_col))
            self.log.info("Use right postfix: radiance, bt")
            self.err_log.error("Cannot convert satellite data: {0}".format(self.use_cal_col))
        
        # Raw와 같은 data type으로 변환
        sat_array = np.clip(sat_array, 0, raw_max)
        sat_array = sat_array.astype(getattr(np, self.raw_dtype))
        
        return sat_array
