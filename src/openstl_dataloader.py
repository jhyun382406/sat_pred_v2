import numpy as np
import dask.array as da
import torch
from dask.distributed import Client


def worker_init(worker_id, worker_seeding='all'):
    """
    openstl/datasets/utils.py 함수 복사.
    """
    worker_info = torch.utils.data.get_worker_info()
    assert worker_info.id == worker_id
    assert worker_seeding in ('all', 'part')
    # random / torch seed already called in dataloader iter class w/ worker_info.seed
    # to reproduce some old results (same seed + hparam combo), partial seeding
    # is required (skip numpy re-seed)
    if worker_seeding == 'all':
        np.random.seed(worker_info.seed % (2 ** 32 - 1))


class SatCollator(object):
    """
    Description:
        Pytorch dataloader에 적용할 collate_fn.
        dask compute를 줄이기 위한 용도.
        batch_datas 외 다른 args를 추가하기 위해 클래스 처리.
        혹시 나올 수 있는 nan도 처리.
        *** dask의 num_workers가 torch dataloader 쪽과 충돌 안 나도록 조정.
    Args:
        in_seq: int, 입력 시퀀스 길이
        num_workers: int, dask에서의 num_workers
    Returns:
        1. 학습
            x: tensor, float32, (batch_size, input_seq_length, channel, height, width)
            y: tensor, float32, (batch_size, output_seq_length, channel, height, width)
        2. 추론
            x: tensor, float32, (batch_size, input_seq_length, channel, height, width)
    """
    def __init__(self, in_seq, num_workers):
        self.in_seq = in_seq
        self.num_workers = num_workers
    
    def __call__(self, batchs):
        seq_data = da.concatenate(
            [d[None, ...] for d in batchs], axis=0
        )
        seq_data = torch.from_numpy(
            seq_data.compute(num_workers=self.num_workers)
        )
        # replace nan to zero (delta value)
        seq_data = torch.nan_to_num(seq_data, nan=0.0)
        x = seq_data[:, :self.in_seq, ...]
        if seq_data.shape[1] > self.in_seq:
            y = seq_data[:, self.in_seq:, ...]
            return x, y
        else:
            return x


class SatCollatorV2(object):
    """
    Description:
        Pytorch dataloader에 적용할 collate_fn.
        dask compute를 줄이기 위한 용도.
            - async 하도록 client 추가하여 진행
        batch_datas 외 다른 args를 추가하기 위해 클래스 처리.
        혹시 나올 수 있는 nan도 처리.
        *** dask의 num_workers가 torch dataloader 쪽과 충돌 안 나도록 조정.
    Args:
        in_seq: int, 입력 시퀀스 길이
        num_workers: int, dask에서의 num_workers
    Returns:
        1. 학습
            x: tensor, float32, (batch_size, input_seq_length, channel, height, width)
            y: tensor, float32, (batch_size, output_seq_length, channel, height, width)
        2. 추론
            x: tensor, float32, (batch_size, input_seq_length, channel, height, width)
    """
    def __init__(self, in_seq, num_workers, log):
        self.in_seq = in_seq
        self.num_workers = num_workers
        # self.client = Client(n_workers=num_workers, asynchronous=True)
        self.client = None
        self.log = log
    
    async def __call__(self, batchs):
        if self.client is None:
            self.client = await Client(
                n_workers=self.num_workers, asynchronous=True
            )
        self.log.info(">>>>>>>>>> {0}".format(self.client))
        seq_data = da.concatenate(
            [d[None, ...] for d in batchs], axis=0
        )
        seq_data = torch.from_numpy(
            await self.client.compute(seq_data)
        )
        # replace nan to zero (delta value)
        seq_data = torch.nan_to_num(seq_data, nan=0.0)
        x = seq_data[:, :self.in_seq, ...]
        if seq_data.shape[1] > self.in_seq:
            y = seq_data[:, self.in_seq:, ...]
            return x, y
        else:
            return x
