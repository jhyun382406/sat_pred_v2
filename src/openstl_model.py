import os
import json
from datetime import datetime
from pytz import timezone

from openstl.utils import create_parser


class OpenSTLModel(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(OpenSTLModel, self).__init__()
        self.cfg = cfg
        
        self.json_path = os.path.join(
            cfg.INFER.LOAD_MODEL_DIR,
            "model_param.json"
        )
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, mode):
        """
        Description:
            OpenSTL 인스턴스 생성.
        Args:
            mode: str, 'train' or 'test'
        Returns:
            args: argparse.Namespace, OpenSTL 설정
        """
        # args = create_parser().parse_args()
        args = create_parser().parse_args([])
        if mode == "train":
            # Setting OpenSTL configs
            training_config, model_config = self.make_model_cfg()
            config = args.__dict__
            config.update(training_config)
            config.update(model_config)
        elif mode == "test":
            loaded_config = self.load_model_cfg(self.json_path)
            loaded_config = self.modified_infer_cfg(loaded_config)
            config = args.__dict__
            config.update(loaded_config)
            config["test"] = True
        self.log.info("Setting OpenSTL configs")
        return args
    
    def make_model_cfg(self):
        """
        Description:
            OpenSTL에 넣어야 할 configs 생성.
        Args:
            mode: str, 'train' or 'test'
        Returns:
            training_config: dict, 학습 설정
            model_config: dict, 모델 설정
        """
        cfg_d = self.cfg.PARAMS.DATA
        cfg_tr = self.cfg.TRAIN
        cfg_opt = self.cfg.PARAMS.OPT
        cfg_m = self.cfg.PARAMS.MODEL
        cfg_l = self.cfg.PARAMS.LOSS
        
        input_shape = cfg_d.INPUT_SHAPE
        now_kst = datetime.now(timezone("Asia/Seoul"))
        now_kst_date = now_kst.strftime("%Y%m%d")[2:]
        
        gpu_nums = len(self.cfg.SETTINGS.IDX_GPUS)
        batch_size = self.cfg.TRAIN.BATCH_SIZE
        ex_name = cfg_tr.MODEL_NAME.format(cfg_d.CHANNEL, now_kst_date)
        
        training_config = {
            'pre_seq_length': cfg_d.INPUT_SEQ,
            'aft_seq_length': cfg_d.OUTPUT_SEQ,
            'total_length': cfg_d.INPUT_SEQ + cfg_d.OUTPUT_SEQ,
            'batch_size': batch_size * gpu_nums,
            'val_batch_size': batch_size * gpu_nums,
            'epoch': cfg_tr.EPOCH,
            'lr': float(cfg_opt.LR) * gpu_nums,
            'metrics': ['rmse', 'mae'],
        
            'res_dir': cfg_tr.UPPER_DIR.format(cfg_d.CHANNEL),
            'ex_name': ex_name,
            'dataname': 'custom',
            'in_shape': [cfg_d.INPUT_SEQ, 1, input_shape[0], input_shape[1]],
            'use_gpu': True,
            # 'use_gpu_idx': 2,
            'seed': self.cfg.SETTINGS.RANDOM_SEED,
            'num_workers': self.cfg.DATALOADER.NUM_WORKERS,
            'fp16': self.cfg.SETTINGS.FP16,
            'clip_mode': cfg_m.CLIP_MODE,   # Gradient clipping
            'clip_grad': cfg_m.CLIP_GRAD,   # Gradient clipping
            # 'use_prefetcher': True,   # prefetch
            
            'sched': cfg_opt.SCHEDULE,
            'warmup_epoch': cfg_tr.WARMUP_EPOCH,
            
            # model resume
            'resume_from': cfg_tr.IMPORT_CKPT_PATH
        }
        
        model_config = {
            # For MetaVP models, the most important hyperparameters are: 
            # N_S, N_T, hid_S, hid_T, model_type
            'method': cfg_m.METHOD,
            # Users can either using a config file or directly set these hyperparameters 
            # 'config_file': 'configs/custom/example_model.py',
            
            # Here, we directly set these parameters
            'model_type': cfg_m.MODEL_TYPE,
            'N_S': cfg_m.N_S,
            'N_T': cfg_m.N_T,
            'hid_S': cfg_m.HID_S,
            'hid_T': cfg_m.HID_T,
            'spatio_kernel_enc': cfg_m.SPATIO_KERNEL_ENC,
            'spatio_kernel_dec': cfg_m.SPATIO_KERNEL_DEC,
            'alpha': cfg_m.ALPHA,   # tau 에 필요 (Loss에 반영하는 Regularizer의 계수)
            
            'drop_path': cfg_m.DROP_PATH,
            
            # # Custom loss
            # 'loss_amp': cfg_l.LOSS_AMP,
            # 'reg_amp': cfg_l.REGULARIZER_AMP
        }
        
        return training_config, model_config
    
    def load_model_cfg(self, json_path):
        """
        Description:
            기존 학습했던 OpenSTL configs 불러오기.
        Args:
            json_path: str, model_params json 파일 경로
        Returns:
            loaded_config: dict, 불러온 모델 설정
        """
        try:
            with open(json_path, "r", encoding="utf-8") as f:
                loaded_config = json.load(f)
        except Exception as e:
            self.log.info("Cannot Load trained model configs")
            self.err_log.error("Cannot Load trained model configs: {0}".format(e))
        else:
            self.log.info("Load trained model configs")
            return loaded_config
    
    def modified_infer_cfg(self, loaded_config):
        """
        Description:
            추론에 반영할 설정으로 수정.
        Args:
            loaded_config: dict, 불러온 모델 설정
        Returns:
            loaded_config: dict, 수정한 모델 설정
        """
        gpu_nums = len(self.cfg.SETTINGS.IDX_GPUS)
        batch_size = self.cfg.TEST.BATCH_SIZE
        ex_name = "test"
        
        modified_config = {
            'val_batch_size': batch_size * gpu_nums,
            'ex_name': ex_name,
            'seed': self.cfg.SETTINGS.RANDOM_SEED,
            'num_workers': self.cfg.DATALOADER.NUM_WORKERS,
        }
        loaded_config.update(modified_config)
        
        return loaded_config
