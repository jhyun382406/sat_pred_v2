import os
import time
import datetime
import logging
import logging.config
import pickle
from collections import namedtuple
from pytz import timezone
import numpy as np
import pandas as pd
import dask.array as da
import zarr
import ray

from utils.log_module import print_elapsed_time
from utils.code_utils import return_dir


class CustomFormatter(logging.Formatter):
    """
    Description:
        KST 시간대를 사용자 정의 포맷으로 변경.
    """
    def converter(self, timestamp):
        """
        Description:
            KST로 시간대 변경.
        Args:
            timestamp: timestamp, UTC 기준 시간대
        Returns:
            dt: timestamp, KST로 변경한 시간대
        """
        dt = datetime.datetime.fromtimestamp(timestamp)
        return dt.astimezone(timezone("Asia/Seoul"))

    def formatTime(self, record, datefmt=None):
        """
        Description:
            사용자 정의 포맷으로 시간대 변경.
        Args:
            record: record, UTC 기준 시간대 객체
            datefmt: str, 문자열로 시간대를 변경시킬 포맷 (default=None)
        Returns:
            s: str, 사용자 정의 포맷으로 변경한 시간대
        """
        dt = self.converter(record.created)
        if datefmt:
            s = dt.strftime(datefmt)
        else:
            try:
                s = dt.isoformat(timespec="milliseconds")
            except TypeError:
                s = dt.isoformat()
        return s


log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": CustomFormatter,
            "format": "[%(levelname)s][%(filename)s:%(lineno)s][%(asctime)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "default",
            "level": "DEBUG"
        }
    },
    "root": {"handlers": ["console"], "level": "WARNING"},   # 최상위 logger에 handler 연결
    "loggers": {
        "infolog": {"handlers": ["console"], "level": "INFO", "propagate": False},
        "infolog.debug": {"handlers": ["console"], "level": "DEBUG", "propagate": False},
        "infolog.error": {"handlers": ["console"], "level": "ERROR", "propagate": False}
    },   # 특정 모듈 logger
}
logging.config.dictConfig(log_config)


def get_kst_times(data_paths, prep):
    # Extract KST time
    names = [os.path.splitext(dat_name)[0] for dat_name in data_paths]
    name_splits = [name.split("_") for name in names]
    kst_start_times = [splits[-2] for splits in name_splits]
    kst_end_times = [splits[-1] for splits in name_splits]
    # Make time list
    total_kst_time_list = list()
    for kst_start_time, kst_end_time in zip(kst_start_times, kst_end_times):
        kst_time_list = prep.sat._make_time_list(
            kst_start_time,
            kst_end_time,
            "min",
            prep.time_step
        )
        total_kst_time_list.extend(kst_time_list)
    return total_kst_time_list


def load_calibration_table(
        calibration_dir,
        cal_table_file,
        cal_table_dtype,
        use_cal_col,
        log,
        err
    ):
    """
    Description:
        GK2A Calibration table 불러오기.
        가시 ~ 근적외: Radiance, Albedo
        단파적외 ~ 적외: Radiance, Brightness Temperature
    Args:
        calibration_dir: str, Calibration 파일 있는 디렉트뢰
        cal_table_file: str, Calibration 파일명
        cal_table_dtype: ,
        use_cal_col,
        log: logger, 로그 객체
        err: logger, 에러로그 객체
    Returns:
        calibration_table: numpy array, 채널의 Calibration table
    """
    calibration_table_path = os.path.join(
        calibration_dir,
        cal_table_file
    )
    try:
        calibration_table = pd.read_csv(
            calibration_table_path,
            encoding="utf-8",
            dtype=cal_table_dtype
        )
    except Exception as e:
        log.info("Load csv file incompletely: {0}".format(cal_table_file))
        err.error("Load csv file incompletely: {0}".format(cal_table_file))
        err.error("{0}".format(e))
        raise e
    else:
        calibration_table = calibration_table[use_cal_col].to_numpy()
        return calibration_table


def load_latlon(
        calibration_dir,
        latlon_file,
        height,
        width,
        log,
        err
    ):
    """
    Description:
        위/경도 파일 불러오기.
    Args:
        None
    Returns:
        latlon: numpy array, 채널의 해상도에 맞는 위/경도
            - 500m: (3600, 3600, 2)
            - 1km: (1800, 1800, 2)
            - 2km: (900, 900, 2)
    """
    latlon_path = os.path.join(calibration_dir, latlon_file)
    try:
        latlon = np.loadtxt(latlon_path, delimiter="\t", dtype=float)
        latlon = latlon.reshape(height, width, -1)
    except Exception as e:
        log.info("Load txt file incompletely: {0}".format(latlon_file))
        err.error("Load txt file incompletely: {0}".format(latlon_file))
        err.error("{0}".format(e))
        raise e
    else:
        log.info("Latitude & Longitude Resolution: {0}".format(latlon.shape))
        return latlon


def _latlon_to_pixel(lat, lon, latlon):
    """
    Description:
        위/경도로 해당 해상도의 픽셀 위치 계산.
        L2 norm이 가장 작은 위치 픽셀을 반환.
    Args:
        lat: float, 위도
        lon: float, 경도
        latlon: numpy array, 채널의 해상도에 맞는 위/경도
    Returns:
        latlon_pixel: tuple, 위/경도 픽셀, (위도, 경도)
    """
    near_latlon = latlon[
        (latlon[..., 0] > lat - 0.1) &
        (latlon[..., 0] < lat + 0.1) &
        (latlon[..., 1] > lon - 0.1) &
        (latlon[..., 1] < lon + 0.1)
    ]
    latlon_tile = np.tile((lat, lon), reps=[near_latlon.shape[0], 1])
    latlon_l2 = np.linalg.norm(near_latlon - latlon_tile, axis=-1)
    lat_adapt, lon_adapt = near_latlon[latlon_l2.argmin()]
    lat_pix, lon_pix = np.where(
        (latlon[..., 0] == lat_adapt) &
        (latlon[..., 1] == lon_adapt)
    )   # 각각 array로 결과 산출됨
    return (lat_pix[0], lon_pix[0])


def make_effective_area_pixel_center(
        latlon,
        center_lat, center_lon,
        half_height, half_width
    ):
    """
    Description:
        위/경도로 해당 해상도의 픽셀 위치 계산.
        가운데 위경도를 기준으로 정사각 영역 픽셀 계산.
    Args:
        latlon: numpy array, 채널의 해상도에 맞는 위/경도
    Returns:
        effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
    """
    # 위/경도 -> 픽셀
    center_pixel = _latlon_to_pixel(center_lat, center_lon, latlon)
    # 직사각형 영역 계산
    lat_pixel_min = center_pixel[0] - half_height
    lat_pixel_max = center_pixel[0] + half_height
    lon_pixel_min = center_pixel[1] - half_width
    lon_pixel_max = center_pixel[1] + half_width
    effective_area_pixel = (lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max)
    return effective_area_pixel


def save_zarr(store_kind, save_path, zarr_shape, grp_sat, grp_kst):
    """
    Description:
        zarr를 저장.
        group 등 기타 옵션은 init에서 self로 가져오기.
        subgroup은 <grp>.create_group("<subgrp_name>") 으로 생성.
        zarr_sat
            └─ sat_ary: zarr array, (time_list_len, h, w)
            └─ kst_dt: zarr array, (time_list_len,)
    Args:
        store_kind: str, 저장방식 (zip, dir)
        save_path: str, 파일 저장 경로
        zarr_shape: tuple, 저장할 레이더 shape
        grp_sat: namedtuple, Zarr 위성 설정 
        grp_kst: namedtuple, Zarr 시간대 설정
    Returns:
        zarr_sat: zarr group, 저장할 zarr 그룹
        store: zarr ZipStore, 파일 저장 인스턴스
    """
    if store_kind == "zip":
        save_path = "{0}.zip".format(save_path)
        store = zarr.ZipStore(save_path, mode="w")
    elif store_kind == "dir":
        store = zarr.DirectoryStore(save_path)
    else:
        store = zarr.DirectoryStore(save_path)
    zarr_sat = zarr.group(store=store)
    
    # Satellite
    rdr_chunk = list(zarr_shape)
    rdr_chunk[0] = grp_sat.CHUNK
    zarr_sat.zeros(
        grp_sat.NAME,
        shape=zarr_shape,
        chunks=tuple(rdr_chunk),
        dtype=grp_sat.DTYPE,
        overwrite=True,
        # compression="none",   # 압축X
    )
    # KST times
    zarr_sat.zeros(
        grp_kst.NAME,
        shape=(zarr_shape[0],),
        chunks=grp_kst.CHUNK,
        dtype=grp_kst.DTYPE,
        overwrite=True,
    )
    
    return zarr_sat, store


def load_zarr(save_path):
    """
    Description:
        저장한 zarr 불러오기.
    Args:
        save_path: str, 파일 저장 경로
    Returns:
        zarr_sat: zarr group, 저장할 zarr 그룹
        store: zarr ZipStore, 파일 저장 인스턴스
    """
    if save_path.endswith("zip"):
        store = zarr.ZipStore(save_path, mode="r")
    else:
        store = zarr.DirectoryStore(save_path)
    zarr_sat = zarr.group(store=store)
    return zarr_sat, store


def quit_zarr(store):
    """
    Description:
        저장한 zarr 파일과 연결 해제.
    Args:
        store: zarr ZipStore, 파일 저장 인스턴스
    Returns:
        None
    """
    store.close()


def assign_zarr(zarr_sat,
                store,
                total_rdr,
                kst_time_list,
                grp_sat,
                grp_kst):
    """
    Description:
        데이터를 zarr에 적재.
    Args:
        zarr_sat: zarr group, 저장할 zarr 그룹
        store: zarr ZipStore, 파일 저장 인스턴스
        total_rdr: numpy array, 전체 레이더 배열 (time_list_len, RDR_H, RDR_W, 1)
        kst_time_list: list, KST 문자열 시간대 리스트
        grp_sat: namedtuple, Zarr 위성 설정 
        grp_kst: namedtuple, Zarr 시간대 설정
    Returns:
        None
    """
    kst_dt = getattr(zarr_sat, grp_kst.NAME)
    kst_dt[:] = np.array(kst_time_list)
    log.info("Save KST time list to zarr")
    
    if isinstance(total_rdr, da.Array):
        da.to_zarr(
            arr=total_rdr,
            url=store,
            component=grp_sat.NAME,
            overwrite=True
        )
    else:
        rdr_ary = getattr(zarr_sat, grp_sat.NAME)
        rdr_ary[:] = total_rdr
    log.info("Save Radar array to zarr")


def load_zarr_dask(load_files, grp_sat, grp_kst):
    """
    Description:
        저장한 zarr를 dask로 불러오기.
    Args:
        load_files: list, 불러올 파일 리스트
        grp_sat: namedtuple, Zarr 위성 설정 
        grp_kst: namedtuple, Zarr 시간대 설정
    Returns:
        sat_total: dask array, 위성 dask 배열
        kst_total: numpy array, KST시간대 배열
    """
    sat_total = [
        da.from_zarr(_zarr, component=grp_sat.NAME)
        for _zarr in load_files
    ]
    kst_total = [
        da.from_zarr(_zarr, component=grp_kst.NAME)
        for _zarr in load_files
    ]
    sat_total = da.concatenate(sat_total, axis=0)
    kst_total = da.concatenate(kst_total, axis=0)
    kst_total = kst_total.compute()
    log.info("Total Sat zarr: {0} / {1}".format(sat_total.shape, sat_total.dtype))
    return sat_total, kst_total


def save_sat_npy(
    idx, one_memmap, save_path
    ):
    if one_memmap.dtype == np.float32:
        np.save(save_path.format(idx), one_memmap)
    else:
        raise TypeError("Not float32: {0} ::: index = {1}".format(one_memmap.dtype, idx))


@ray.remote
def ray_save_sat_npy(
        idx, one_memmap, save_path
    ):
    save_sat_npy(idx, one_memmap, save_path)


def main(
        data_paths, shape, upper_dir, chn, filename, ray_cpus, prep, log
    ):
    # # ray start
    # if not ray.is_initialized():
    #     ray.init(num_cpus=ray_cpus)
    
    # save npy
    for data_path, sp in zip(data_paths, shape):
        s_time = time.time()
        log.info("Load: {0}".format(data_path.split("/")[-1]))
        dir_name = os.path.splitext(os.path.basename(data_path))[0]
        dir_name = "npy_{0}".format(dir_name)
        save_dir = return_dir(True, upper_dir, dir_name)
        save_path = os.path.join(save_dir, filename)
        log.info("Save dir: {0}".format(save_dir))
        # log.info("Save path: {0}".format(save_path))
        
        # kst times
        kst_time_list = get_kst_times([data_path], prep)
        # memmap -> dask array
        memmap = np.memmap(
            data_path, dtype=np.float32, shape=tuple(sp), mode="r", order="C"
        )
        log.info("Array: {0} /// KST: {1}".format(memmap.shape, len(kst_time_list)))
        
        dask_ary = da.from_array(memmap)
        dask_ary = da.rechunk(
            dask_ary, chunks=(1, dask_ary.shape[1], dask_ary.shape[1])
        )
        
        # save npy
        da.to_npy_stack(save_dir, dask_ary, axis=0)
        
        ###########################################
        # 그냥 dask.array.to_npy_stack 으로 npy 만들기 (axis=0)
        ###########################################
        
        # # using ray
        # ray_save_path = ray.put(save_path)
        # sat_ids = [
        #     # @ray.remote에는 self도 argument로 넣어줘야 함
        #     ray_save_sat_npy.remote(
        #         idx, one_memmap, ray_save_path
        #     )
        #     for idx, one_memmap
        #     in enumerate(memmap)
        # ]
        # while len(sat_ids):
        #     done, sat_ids = ray.wait(sat_ids)   # 완료된건 done으로 감
        #     ray_result = ray.get(done[0])
        #     # log.info(">>> done: {0} {1} /// remain: {2} {3}", type(done), len(done), type(sat_ids), len(sat_ids))
        
        # dask.to_npy_stack이 아니라면 info를 강제로 만들어줘야 함
        # dask.from_npy_stack으로 읽으려면 npy 경로에 info가 필수
        # info는 dictionary로 pickle로 저장됨
        # da_chunks = (
        #     (tuple([1 for _ in range(memmap.shape[0])])),
        #     (memmap.shape[1],),
        #     (memmap.shape[2],)
        # )
        # da_info = {
        #     "chunks": da_chunks,
        #     "dtype": np.float32,
        #     "axis": 0
        # }
        # with open(os.path.join(save_dir, "info"), "wb") as f:
        #     pickle.dump(da_info, f)
        
        h, m, s = print_elapsed_time(s_time)
        log.info("Save npy completely: {0} hours {1} mins {2:.2f} secs".format(h, m, s))
        log.info("="*50)
    
    # # ray stop
    # if not ray.is_initialized():
    #     ray.shutdown()


def main_dat2zarr(
        data_paths,
        shape,
        calib_cfgs,
        input_shape,
        grp_sat,
        grp_kst,
        zarr_store_kind,
        upper_dir,
        log,
        err
    ):
    calib_table = load_calibration_table(
        calib_cfgs["calibration_dir"],
        calib_cfgs["cal_table_file"],
        calib_cfgs["cal_table_dtype"],
        calib_cfgs["use_cal_col"],
        log,
        err
    )
    latlon = load_latlon(
        calib_cfgs["calibration_dir"],
        calib_cfgs["latlon_file"],
        calib_cfgs["latlon_resolution"][0],
        calib_cfgs["latlon_resolution"][1],
        log,
        err
    )
    USE_CROP = calib_cfgs["use_crop"]   # 너무 크면 center crop
    if USE_CROP:
        effective_area_pixel = make_effective_area_pixel_center(
            latlon,
            calib_cfgs["center_lat"],
            calib_cfgs["center_lon"],
            input_shape[0] // 2,
            input_shape[1] // 2
        )
        lat_px_min, lat_px_max, lon_px_min, lon_px_max = effective_area_pixel
        log.info("Crop pixels (Height): {0} ~ {1}".format(lat_px_min, lat_px_max))
        log.info("Crop pixels (Width): {0} ~ {1}".format(lon_px_min, lon_px_max))
    
    # save zarr
    for data_path, sp in zip(data_paths, shape):
        s_time = time.time()
        log.info("Load: {0} / {1}".format(data_path.split("/")[-1], sp))
        dir_name = os.path.splitext(os.path.basename(data_path))[0]
        if calib_cfgs["use_calib"]:
            if USE_CROP:
                dir_name = dir_name.replace(chn, "{0}_calib_center{1}".format(chn, input_shape[0]))
            else:
                dir_name = dir_name.replace(chn, "{0}_calib".format(chn))
        dir_name = "zarr_{0}".format(dir_name)
        save_path = return_dir(True, upper_dir, dir_name)
        log.info("Save directory: {0}".format(save_path))
        
        # kst times
        kst_time_list = get_kst_times([data_path], prep)
        
        # memmap -> calibration -> dask array
        memmap = np.memmap(
            data_path, dtype=np.uint16, shape=tuple(sp), mode="r", order="C"
        )
        if calib_cfgs["use_calib"] and USE_CROP:
            calib_datas = calib_table[memmap[:, lat_px_min:lat_px_max, lon_px_min:lon_px_max]]
        elif calib_cfgs["use_calib"]:
            calib_datas = calib_table[memmap]
        else:
            calib_datas = memmap
        log.info("Calibrate: {0} / dtype: {1} / shape: {2}".format(calib_cfgs["use_cal_col"], calib_datas.dtype, calib_datas.shape))
        dask_ary = da.from_array(calib_datas)
        dask_ary = da.rechunk(
            dask_ary, chunks=(grp_sat.CHUNK, dask_ary.shape[1], dask_ary.shape[1])
        )
        
        # Zarr
        zarr_sat, store = save_zarr(
            zarr_store_kind, save_path, dask_ary.shape, grp_sat, grp_kst
        )
        assign_zarr(
            zarr_sat, store,
            dask_ary, kst_time_list,
            grp_sat, grp_kst
        )
        quit_zarr(store)
        h, m, s = print_elapsed_time(s_time)
        log.info("Save zarr completely: {0} hours {1} mins {2:.2f} secs".format(h, m, s))
        log.info("="*50)


if __name__ == "__main__":
    from setproctitle import setproctitle
    setproctitle("zarr_test")
    
    log = logging.getLogger("infolog")
    deb = logging.getLogger("infolog.debug")
    err = logging.getLogger("infolog.error")
    
    from utils.code_utils import convert
    from src.openstl_preprocess import SatPrepV2
    
    cfg_file = "./configs/openstl_ir112_cfg.yaml"
    cfg = convert(cfg_file)
    prep = SatPrepV2(cfg, log, err)
    
    # configs
    upper_dir = "/data/gk2a_ary_split"
    chn = cfg.PARAMS.DATA.CHANNEL
    # filename = "{0}_{1}.npy"   # channel, kst_time
    filename = "{0}"   # dask.from_npy_stacks 에서는 인덱스 주소로 판단
    ray_cpus = 16

    data_paths = [
        "/data/gk2a_ary/gk2a_ir112_202203010006_202205312356.dat",
        "/data/gk2a_ary/gk2a_ir112_202209010006_202211302356.dat",
    ]
    shape = [
        [13248, 900, 900],
        [13104, 900, 900],
    ]
    
    # calibrataion & zarr configs
    calib_cfgs = {
        "calibration_dir": cfg.RAWDATA.CALIBRATION_DIR,
        "cal_table_file": cfg.RAWDATA.CAL_TABLE_FILE,
        "cal_table_dtype": cfg.RAWDATA.CAL_TABLE_DTYPE,
        "use_cal_col": cfg.PARAMS.DATA.CALIBRATION.USE_COL,
        "use_calib": cfg.PARAMS.DATA.CALIBRATION.USE,
        "latlon_file": cfg.RAWDATA.LATLON_FILE,
        "latlon_resolution": cfg.RAWDATA.LATALON_RESOLUTION,
        "use_crop": False,
        "center_lat": cfg.PARAMS.DATA.CROP_AREA.CENTER_LAT,
        "center_lon": cfg.PARAMS.DATA.CROP_AREA.CENTER_LON,
    }
    input_shape = (900, 900)
    grp_sat = cfg.TRAIN.ZARR_GROUP.SAT
    grp_kst = cfg.TRAIN.ZARR_GROUP.KST
    zarr_store_kind = cfg.TRAIN.ZARR_GROUP.STORE
    
    # save npy
    # main(
    #     data_paths, shape, upper_dir, chn, filename, ray_cpus, prep, log
    # )
    main_dat2zarr(
        data_paths, shape,
        calib_cfgs, input_shape,
        grp_sat, grp_kst, zarr_store_kind, upper_dir,
        log, err
    )
    