import time
import datetime
import logging
import logging.config
from functools import partial
from itertools import repeat
from pytz import timezone
import numpy as np
import pandas as pd
import dask.array as da
import torch
from torch.utils.data import (
    Dataset, DataLoader,
    RandomSampler, BatchSampler, SequentialSampler
)

from utils.log_module import print_elapsed_time
from src.openstl_dataloader import SatCollator, SatCollatorV2


class CustomFormatter(logging.Formatter):
    """
    Description:
        KST 시간대를 사용자 정의 포맷으로 변경.
    """
    def converter(self, timestamp):
        """
        Description:
            KST로 시간대 변경.
        Args:
            timestamp: timestamp, UTC 기준 시간대
        Returns:
            dt: timestamp, KST로 변경한 시간대
        """
        dt = datetime.datetime.fromtimestamp(timestamp)
        return dt.astimezone(timezone("Asia/Seoul"))

    def formatTime(self, record, datefmt=None):
        """
        Description:
            사용자 정의 포맷으로 시간대 변경.
        Args:
            record: record, UTC 기준 시간대 객체
            datefmt: str, 문자열로 시간대를 변경시킬 포맷 (default=None)
        Returns:
            s: str, 사용자 정의 포맷으로 변경한 시간대
        """
        dt = self.converter(record.created)
        if datefmt:
            s = dt.strftime(datefmt)
        else:
            try:
                s = dt.isoformat(timespec="milliseconds")
            except TypeError:
                s = dt.isoformat()
        return s


log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": CustomFormatter,
            "format": "[%(levelname)s][%(filename)s:%(lineno)s][%(asctime)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "default",
            "level": "DEBUG"
        }
    },
    "root": {"handlers": ["console"], "level": "WARNING"},   # 최상위 logger에 handler 연결
    "loggers": {
        "infolog": {"handlers": ["console"], "level": "INFO", "propagate": False},
        "infolog.debug": {"handlers": ["console"], "level": "DEBUG", "propagate": False},
        "infolog.error": {"handlers": ["console"], "level": "ERROR", "propagate": False}
    },   # 특정 모듈 logger
}

logging.config.dictConfig(log_config)


class TestDataset(Dataset):
    def __init__(
        self,
        datas=None,
        seq_indices=None,
        in_seq: int=None,
        out_seq: int=None,
        sat_mean: np.ndarray=None,
        sat_std: np.ndarray=None,
        ):
        self.datas = datas
        self.target_indices=seq_indices
        self.in_seq = in_seq
        self.out_seq = out_seq
        self.total_seq = in_seq + out_seq
        self.mean = sat_mean
        self.std = sat_std
    
    def __len__(self):
        return len(self.target_indices)
    
    def __getitem__(self, index):
        map_index = self.target_indices[index]
        seq_data = self.datas[map_index: map_index + self.total_seq]
        seq_data = self._hwc_to_chw(seq_data)
        seq_data = self._normalize(seq_data)
        return seq_data
        # x = self._ary_to_tensor(seq_data[:self.in_seq, ...])
        # y = self._ary_to_tensor(seq_data[self.in_seq:, ...])
        # del seq_data
        # return x, y
    
    def _hwc_to_chw(self, seq_data):
        """
        Description:
            (height, width, channel) -> (channel, height, width).
        Args:
            seq_data: dask array, (seq_length, height, width, channel)
        Returns:
            seq_data: dask array, (seq_length, channel, height, width)
        """
        if len(seq_data.shape) == 3:
            seq_data = seq_data[..., None]
        return seq_data.transpose(0, 3, 1, 2)
    
    def _cvt_float32(self, seq_data):
        return (seq_data/1000).astype(np.float32)
    
    def _normalize(self, seq_data):
        return (seq_data - self.mean) / self.std
    
    # def _ary_to_tensor(self, seq_data):
    #     """
    #     Description:
    #         Array를 tensor로 변환.
    #     Args:
    #         seq_data: dask array, (seq_length, channel, height, width)
    #     Returns:
    #         seq_data: torch.tensor, (seq_length, channel, height, width)
    #     """
    #     return torch.from_numpy(seq_data).float()


class TestDatasetV2(TestDataset):
    def __init__(
        self,
        datas=None,
        seq_indices=None,
        in_seq: int=None,
        out_seq: int=None,
        sat_mean: np.ndarray=None,
        sat_std: np.ndarray=None,
        ):
        super(TestDatasetV2, self).__init__(
            datas, seq_indices, in_seq, out_seq, sat_mean, sat_std
        )
    
    def __getitem__(self, index):
        map_index = self.target_indices[index]
        seq_data = self.datas[map_index: map_index + self.total_seq + 1]
        seq_data = seq_data[1:] - seq_data[:-1]   # make delta
        seq_data = self._hwc_to_chw(seq_data)
        seq_data = self._normalize(seq_data)
        return seq_data


def worker_init(worker_id, worker_seeding='all'):
    worker_info = torch.utils.data.get_worker_info()
    assert worker_info.id == worker_id
    assert worker_seeding in ('all', 'part')
    # random / torch seed already called in dataloader iter class w/ worker_info.seed
    # to reproduce some old results (same seed + hparam combo), partial seeding
    # is required (skip numpy re-seed)
    if worker_seeding == 'all':
        np.random.seed(worker_info.seed % (2 ** 32 - 1))


def main_dat(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION):
    # Make sample seq. 20000 으로 만들면 터짐
    s_time = time.time()
    in_seq, out_seq = 12, 12
    # sample_src = np.random.normal(size=(10000,)).astype(np.float32)
    # sample = np.random.choice(sample_src, size=(20000, 900, 900), replace=True)
    # del sample_src
    # sample = da.from_array(sample, chunks=(144, 900, 900))
    data_paths = [
        "/data/gk2a_ary/gk2a_sw038_calib_center_202003010000_202005312350.dat",
        "/data/gk2a_ary/gk2a_sw038_calib_center_202103010000_202105312350.dat",
        "/data/gk2a_ary/gk2a_sw038_calib_center_202203010000_202205312350.dat",
        # "/data/gk2a_ary/gk2a_sw038_calib_center_202203010006_202205312356.dat",
    ]
    shape = [
        [13248, 512, 512],
        [13248, 512, 512],
        [13248, 512, 512],
        # [13248, 512, 512],
    ]
    memmap_list = []
    for data_path, sp in zip(data_paths, shape):
        memmap = np.memmap(
            data_path, dtype=np.float32, shape=tuple(sp), mode="r", order="C"
        )
        memmap_list.append(memmap)
    sample = da.concatenate(memmap_list, axis=0)
    sample = da.rechunk(
        sample,
        chunks=(144, sample.shape[1], sample.shape[2])
    )
    seq_indices = np.arange(len(sample)-(in_seq+out_seq)+1)
    
    # make delta
    if VERSION == "V1":
        sample = sample[1:] - sample[:-1]
    # seq indices
    if VERSION == "V1":
        seq_indices = np.arange(len(sample)-(in_seq+out_seq)+1)
    elif VERSION == "V2":
        seq_indices = np.arange(len(sample)-(in_seq+out_seq)+1-1)   # delta 반영
    seq_indices = np.random.choice(seq_indices, size=12000, replace=False)
    log.info(">>> sample: {0} / {1}".format(sample.shape, sample.dtype))
    log.info(">>> seq_indices: {0}".format(seq_indices.shape))
    
    # mean & standard deviation
    sat_mean = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_mean.npy")
    sat_std = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_std.npy")
    sat_mean = sat_mean[194:194+512, 194:194+512]
    sat_std = sat_std[194:194+512, 194:194+512]
    log.info(">>> mean: {0} / std: {1}".format(sat_mean.shape, sat_std.shape))
    h, m, s = print_elapsed_time(s_time)
    log.info(">>> Make sample seq. ::: {0} hours {1} mins {2:.2f} secs".format(h, m, s))
    
    # # calibration
    # calib_path = "/data/sat_pred/calibration_source/gk2a_calibration_fogdetect_v3.csv"
    # calib_table = pd.read_csv(
    #     calib_path, encoding="utf-8", dtype=np.float32
    # )
    # calib_table = calib_table["sw038_bt"].to_numpy()
    # log.info(">>> Calibration table: {0}".format(calib_table.shape))
    # sample = calib_table[sample]
    # sample = da.from_array(
    #     sample,
    #     chunks=(144, sample.shape[1], sample.shape[2])
    # )
    # log.info(">>> Calibration result: {0}".format(sample))
    
    # sample dataset test
    if VERSION == "V1":
        sample_ds = TestDataset(sample, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    elif VERSION == "V2":
        sample_ds = TestDatasetV2(sample, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    tmptmp = sample_ds[2]
    log.info(">>> One data shape: {0}".format(tmptmp.shape))
    
    sampler = RandomSampler(sample_ds)
    batch_sampler = BatchSampler(
        sampler,
        batch_size=batch_size,
        drop_last=False
    )

    sat_collate = SatCollator(in_seq, DASK_WOKRDERS)
    worker_seeding = "all"
    loader_class = torch.utils.data.DataLoader
    loader_args = dict(
        num_workers=NUM_WORKERS,
        batch_sampler=batch_sampler,
        collate_fn=sat_collate,
        pin_memory=True,
        worker_init_fn=partial(worker_init, worker_seeding=worker_seeding),
    )
    if NUM_WORKERS > 0:
        loader_args["multiprocessing_context"] = "fork"   # test, default: fork
        loader_args["persistent_workers"] = True
    sample_dl = loader_class(sample_ds, **loader_args)
    log.info(">>> dataloader: {0}".format(len(sample_dl)))
    
    device = torch.device("cuda")
    # iterator 방식
    min_list, sec_list = [], []
    s_time = time.time()
    for idx, data in enumerate(sample_dl):
        x, y = data[0].to(device), data[1].to(device)
        # if NUM_WORKERS > 0 and (idx % NUM_WORKERS == 0 or idx == len(sample_dl)-1):
        #     log.info("{0} ::: {1} ({2}) / {3} ({4})".format(idx, x.shape, type(x), y.shape, type(y)))
        h, m, s = print_elapsed_time(s_time)
        if idx !=0:
            min_list.append(m)
            sec_list.append(s)
        # del x, y
        torch.cuda.empty_cache()
        # if idx % 10 == 0 or idx == len(sample_dl)-1:
        #     log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        s_time = time.time()
        
        if idx == 96: break
    
    total_sec_list = [60*_m+_s for _m, _s in zip(min_list, sec_list)]
    total_sec_df = pd.DataFrame({"secs": total_sec_list})
    total_sec_df.to_csv(
        "./dl_times.csv",
        index=False,
        encoding="utf-8",
    )
        
    # # generator 방식
    # for idx in range(len(sample_dl)):
    #     x, y = next(sample_dl)
    #     log.info("{0} ::: {1} ({2}) / {3} ({4})".format(idx, x.shape, type(x), y.shape, type(y)))


def load_zarr_dask(load_files, grp_sat, grp_kst):
    """
    Description:
        저장한 zarr를 dask로 불러오기.
    Args:
        load_files: list, 불러올 파일 리스트
        grp_sat: namedtuple, Zarr 위성 설정 
        grp_kst: namedtuple, Zarr 시간대 설정
    Returns:
        sat_total: dask array, 위성 dask 배열
        kst_total: numpy array, KST시간대 배열
    """
    sat_total = [
        da.from_zarr(_zarr, component=grp_sat.NAME)
        for _zarr in load_files
    ]
    kst_total = [
        da.from_zarr(_zarr, component=grp_kst.NAME)
        for _zarr in load_files
    ]
    sat_total = da.concatenate(sat_total, axis=0)
    kst_total = da.concatenate(kst_total, axis=0)
    kst_total = kst_total.compute()
    log.info("Total Sat zarr: {0} / {1}".format(sat_total.shape, sat_total.dtype))
    return sat_total, kst_total


def main_zarr(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION):
    from collections import namedtuple
    
    s_time = time.time()
    in_seq, out_seq = 12, 12
    data_paths = [
        "/data/gk2a_ary_split/zarr_gk2a_sw038_calib_center_202003010000_202005312350",
        "/data/gk2a_ary_split/zarr_gk2a_sw038_calib_center_202103010000_202105312350",
        "/data/gk2a_ary_split/zarr_gk2a_sw038_calib_center_202203010000_202205312350",
        # "/data/gk2a_ary_split/zarr_gk2a_sw038_calib_center_202203010006_202205312356",
    ]
    # zarr configs
    _grp_sat_dict = {"NAME": "sat_ary", "DTYPE": "f4", "CHUNK": 1}
    _grp_kst_dict = {"NAME": "kst_dt", "DTYPE": "U12", "CHUNK": 4096}
    grp_sat = namedtuple("GenericDict", _grp_sat_dict.keys())(**_grp_sat_dict)
    grp_kst = namedtuple("GenericDict", _grp_kst_dict.keys())(**_grp_kst_dict)
    sat_total, kst_total = load_zarr_dask(data_paths, grp_sat, grp_kst)
    log.info("KST: {0} ~ {1} ({2}) / type: {3}".format(kst_total[0], kst_total[-1], len(kst_total), type(kst_total)))
    log.info("Satellite: {0} / {1} / {2} / Chunksize: {3}".format(type(sat_total), sat_total.shape, sat_total.dtype, sat_total.chunksize))
    
    # make delta
    if VERSION == "V1":
        sat_total = sat_total[1:] - sat_total[:-1]
    
    # seq indices
    if VERSION == "V1":
        seq_indices = np.arange(len(sat_total)-(in_seq+out_seq)+1)
    elif VERSION == "V2":
        seq_indices = np.arange(len(sat_total)-(in_seq+out_seq)+1-1)   # delta 반영
    seq_indices = np.random.choice(seq_indices, size=12000, replace=False)
    log.info(">>> seq_indices: {0}".format(seq_indices.shape))
    
    # mean & standard deviation
    sat_mean = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_mean.npy")
    sat_std = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_std.npy")
    sat_mean = sat_mean[194:194+512, 194:194+512]
    sat_std = sat_std[194:194+512, 194:194+512]
    log.info(">>> mean: {0} / std: {1}".format(sat_mean.shape, sat_std.shape))
    
    # sample dataset test
    if VERSION == "V1":
        sample_ds = TestDataset(sat_total, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    elif VERSION == "V2":
        sample_ds = TestDatasetV2(sat_total, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    tmptmp = sample_ds[2]
    log.info(">>> One data shape: {0}".format(tmptmp.shape))
    
    sampler = RandomSampler(sample_ds)
    batch_sampler = BatchSampler(
        sampler,
        batch_size=batch_size,
        drop_last=False
    )

    sat_collate = SatCollator(in_seq, DASK_WOKRDERS)
    worker_seeding = "all"
    loader_class = torch.utils.data.DataLoader
    loader_args = dict(
        num_workers=NUM_WORKERS,
        batch_sampler=batch_sampler,
        collate_fn=sat_collate,
        pin_memory=True,
        worker_init_fn=partial(worker_init, worker_seeding=worker_seeding),
    )
    if NUM_WORKERS > 0:
        loader_args["multiprocessing_context"] = "fork"   # test, default: fork
        loader_args["persistent_workers"] = True
    sample_dl = loader_class(sample_ds, **loader_args)
    log.info(">>> dataloader: {0}".format(len(sample_dl)))
    
    device = torch.device("cuda")
    # iterator 방식
    min_list, sec_list = [], []
    s_time = time.time()
    for idx, data in enumerate(sample_dl):
        x, y = data[0].to(device), data[1].to(device)
        # if NUM_WORKERS > 0 and (idx % NUM_WORKERS == 0 or idx == len(sample_dl)-1):
        #     log.info("{0} ::: {1} ({2}) / {3} ({4})".format(idx, x.shape, type(x), y.shape, type(y)))
        h, m, s = print_elapsed_time(s_time)
        if idx !=0:
            min_list.append(m)
            sec_list.append(s)
        # del x, y
        torch.cuda.empty_cache()
        # if idx % 10 == 0 or idx == len(sample_dl)-1:
        #     log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        s_time = time.time()
        
        if idx == 96: break
    
    total_sec_list = [60*_m+_s for _m, _s in zip(min_list, sec_list)]
    total_sec_df = pd.DataFrame({"secs": total_sec_list})
    total_sec_df.to_csv(
        "./dl_times.csv",
        index=False,
        encoding="utf-8",
    )


def load_npy_dask(load_files):
    """
    Description:
        저장한 npy stack을 dask로 불러오기.
    Args:
        load_files: list, 불러올 파일 리스트
    Returns:
        sat_total: dask array, 위성 dask 배열
    """
    sat_total = [
        da.from_npy_stack(npy_stack_dir, mmap_mode="r")
        for npy_stack_dir in load_files
    ]
    sat_total = da.concatenate(sat_total, axis=0)
    log.info("Total Sat npy: {0} / {1}".format(sat_total.shape, sat_total.dtype))
    return sat_total


def main_npy(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION):
    from collections import namedtuple
    
    s_time = time.time()
    in_seq, out_seq = 12, 12
    data_paths = [
        "/data/gk2a_ary_split/npy_gk2a_sw038_calib_center_202003010000_202005312350",
        "/data/gk2a_ary_split/npy_gk2a_sw038_calib_center_202103010000_202105312350",
        "/data/gk2a_ary_split/npy_gk2a_sw038_calib_center_202203010000_202205312350",
        # "/data/gk2a_ary_split/npy_gk2a_sw038_calib_center_202203010006_202205312356",
    ]
    sat_total = load_npy_dask(data_paths)
    log.info("Satellite: {0} / {1} / {2} / Chunksize: {3}".format(type(sat_total), sat_total.shape, sat_total.dtype, sat_total.chunksize))
    
    # make delta
    if VERSION == "V1":
        sat_total = sat_total[1:] - sat_total[:-1]
    
    # seq indices
    if VERSION == "V1":
        seq_indices = np.arange(len(sat_total)-(in_seq+out_seq)+1)
    elif VERSION == "V2":
        seq_indices = np.arange(len(sat_total)-(in_seq+out_seq)+1-1)   # delta 반영
    seq_indices = np.random.choice(seq_indices, size=12000, replace=False)
    log.info(">>> seq_indices: {0}".format(seq_indices.shape))
    
    # mean & standard deviation
    sat_mean = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_mean.npy")
    sat_std = np.load("/data/sat_pred/openstl_model_sw038/sw038_tau_240405/sw038_std.npy")
    sat_mean = sat_mean[194:194+512, 194:194+512]
    sat_std = sat_std[194:194+512, 194:194+512]
    log.info(">>> mean: {0} / std: {1}".format(sat_mean.shape, sat_std.shape))
    
    # sample dataset test
    if VERSION == "V1":
        sample_ds = TestDataset(sat_total, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    elif VERSION == "V2":
        sample_ds = TestDatasetV2(sat_total, seq_indices, in_seq, out_seq, sat_mean, sat_std)
    tmptmp = sample_ds[2]
    log.info(">>> One data shape: {0}".format(tmptmp.shape))
    
    sampler = RandomSampler(sample_ds)
    batch_sampler = BatchSampler(
        sampler,
        batch_size=batch_size,
        drop_last=False
    )

    sat_collate = SatCollator(in_seq, DASK_WOKRDERS)
    # sat_collate = SatCollatorV2(in_seq, DASK_WOKRDERS, log)
    worker_seeding = "all"
    loader_class = torch.utils.data.DataLoader
    loader_args = dict(
        num_workers=NUM_WORKERS,
        batch_sampler=batch_sampler,
        collate_fn=sat_collate,
        pin_memory=True,
        worker_init_fn=partial(worker_init, worker_seeding=worker_seeding),
    )
    if NUM_WORKERS > 0:
        loader_args["multiprocessing_context"] = "fork"   # test, default: fork
        loader_args["persistent_workers"] = True
    sample_dl = loader_class(sample_ds, **loader_args)
    log.info(">>> dataloader: {0}".format(len(sample_dl)))
    
    device = torch.device("cuda")
    # iterator 방식
    min_list, sec_list = [], []
    s_time = time.time()
    for idx, data in enumerate(sample_dl):
        x, y = data[0].to(device), data[1].to(device)
        # if NUM_WORKERS > 0 and (idx % NUM_WORKERS == 0 or idx == len(sample_dl)-1):
        #     log.info("{0} ::: {1} ({2}) / {3} ({4})".format(idx, x.shape, type(x), y.shape, type(y)))
        h, m, s = print_elapsed_time(s_time)
        if idx !=0:
            min_list.append(m)
            sec_list.append(s)
        # del x, y
        torch.cuda.empty_cache()
        # if idx % 10 == 0 or idx == len(sample_dl)-1:
        #     log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        log.info("{0} ::: {1} hours {2} mins {3:.2f} secs".format(idx, h, m, s))
        s_time = time.time()
        
        if idx == 96: break
    
    total_sec_list = [60*_m+_s for _m, _s in zip(min_list, sec_list)]
    total_sec_df = pd.DataFrame({"secs": total_sec_list})
    total_sec_df.to_csv(
        "./dl_times.csv",
        index=False,
        encoding="utf-8",
    )


if __name__ == "__main__":
    import gc
    from setproctitle import setproctitle
    
    gc.disable()
    setproctitle("dl_test")
    
    s_time = time.time()
    
    log = logging.getLogger("infolog")
    deb = logging.getLogger("infolog.debug")
    err = logging.getLogger("infolog.error")
    
    batch_size = 4
    NUM_WORKERS = 0
    DASK_WOKRDERS = 8
    VERSION = "V2"
    
    main_dat(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION)
    # main_zarr(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION)
    # main_npy(log, batch_size, NUM_WORKERS, DASK_WOKRDERS, VERSION)
    
    h, m, s = print_elapsed_time(s_time)
    log.info("{0} hours {1} mins {2:.2f} secs".format(h, m, s))
